This is the kernel driver needed for libgannet.

A bitbake recipe is included. If you do not want to use bitbake then the
driver and a makefile can be found in `files`.
