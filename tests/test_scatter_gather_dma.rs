use std::{
    cmp::min,
    path::PathBuf,
    ptr::copy_nonoverlapping,
    sync::atomic::fence,
    sync::atomic::Ordering,
    thread,
    time,
};
use rand::{
    distributions::Uniform,
    Rng,
};

use serial_test_derive::serial;

use gannet::{
    errors::DmaError,
    Operation,
    uni_channel::scatter_gather,
};

// This function generates random values for an MM2S transfer and a
// corresponding S2MM transfer. It also populates the MM2S memory with data
// which will work with the MM2S transfer. It returns all of the arguments
// required to create an MM2S and an S2MM operation along with the expected
// contents of the S2MM memory after the two transfers have been run.
fn setup_random_sg_transfers(
    mm2s_device: &scatter_gather::Device,
    s2mm_device: &scatter_gather::Device) -> (
        usize, usize, usize, usize, usize, usize, Vec<u8>) {

        let mut rng = rand::thread_rng();

        // Get pointers to and the size of the device memories
        let (_mm2s_dma_data, mm2s_dma_data_size) = mm2s_device.get_memory();
        let (_s2mm_dma_data, s2mm_dma_data_size) = s2mm_device.get_memory();

        // Get the smaller of the two memory sizes
        let dma_data_size = min(mm2s_dma_data_size, s2mm_dma_data_size);

        // Need to check how many descriptors are available on the devices
        let n_mm2s_available_descriptors =
            mm2s_device.n_available_descriptors().unwrap();
        let n_s2mm_available_descriptors =
            s2mm_device.n_available_descriptors().unwrap();

        // Get the smaller of the two n_available_descriptors
        let n_available_descriptors =
            min(n_mm2s_available_descriptors, n_s2mm_available_descriptors);

        // Generate a random value for the block_size
        let block_size = 8*rng.gen_range(1, (1 << 16)/8);

        // Determine the maximum number of blocks that can fit within the
        // memory
        let n_available_mem_blocks = dma_data_size/block_size;

        // Get the smaller of n_available_mem_blocks and
        // n_available_descriptors as the smaller of these two values
        // determines the maximum number of blocks we can have.
        let physical_constraint_on_n_blocks =
            min(n_available_mem_blocks, n_available_descriptors);

        // Set an upper limit of 40 on the max_n_blocks
        let max_n_blocks = min(40, physical_constraint_on_n_blocks);

        // Randomly set n_blocks
        let n_blocks = rng.gen_range(1, max_n_blocks + 1);

        // Generate a random value for the block strides
        let mm2s_block_stride = 8*rng.gen_range(
            block_size/8, (mm2s_dma_data_size/n_blocks)/8 + 1);
        let s2mm_block_stride = 8*rng.gen_range(
            block_size/8, (s2mm_dma_data_size/n_blocks)/8 + 1);

        // Generate a random source and destination offset
        let mm2s_offset =
            8*rng.gen_range(
                0, (mm2s_dma_data_size - mm2s_block_stride*n_blocks)/8);
        let s2mm_offset =
            8*rng.gen_range(
                0, (s2mm_dma_data_size - s2mm_block_stride*n_blocks)/8);

        let expected_s2mm_mem =  gen_and_write_random_data(
            &mm2s_device, &s2mm_device, &block_size, &mm2s_block_stride,
            &n_blocks, &mm2s_offset, &block_size, &s2mm_block_stride,
            &n_blocks, &s2mm_offset,);

        (n_blocks, block_size, mm2s_block_stride, mm2s_offset,
         s2mm_block_stride, s2mm_offset, expected_s2mm_mem)
}

// Given operation arguments this function will set up the MM2S memory with
// random data that satisfies those arguments and will return the expected
// S2MM memory.
//
// Note: This function does not check that the arguments will be valid. That
// must be performed in the calling function
fn gen_and_write_random_data (
    mm2s_device: &scatter_gather::Device,
    s2mm_device: &scatter_gather::Device,
    mm2s_block_size: &usize,
    mm2s_block_stride: &usize,
    mm2s_n_blocks: &usize,
    mm2s_offset: &usize,
    s2mm_block_size: &usize,
    s2mm_block_stride: &usize,
    s2mm_n_blocks: &usize,
    s2mm_offset: &usize,) -> Vec<u8> {

    let mut rng = rand::thread_rng();
    let range = Uniform::new(0, 1u16 << 8);

    // Get pointers to and the size of the device memories
    let (mm2s_dma_data, _mm2s_dma_data_size) = mm2s_device.get_memory();
    let (s2mm_dma_data, s2mm_dma_data_size) = s2mm_device.get_memory();

    // Create a vector to overwrite the destination before beginning the
    // transfer
    let destination_setup_data = vec!(0u8; s2mm_dma_data_size);

    // Need the memory fences here to prevent the cpu from reordering
    // the non volatile reads/writes around volatile reads/writes
    fence(Ordering::SeqCst);
    unsafe {
        // Set up the destination memory with known values so we know
        // the state of the memory before the transfer
        copy_nonoverlapping(
            destination_setup_data.as_ptr(),
            s2mm_dma_data.offset(0 as isize), s2mm_dma_data_size);
    }
    fence(Ordering::SeqCst);

    // Create a vector to take the raw data
    let mut raw_data = Vec::new();

    for block_num in 0..*mm2s_n_blocks {
        // Generate random test data
        let test_data: Vec<u8> =
            (0..*mm2s_block_size).map(|_| rng.sample(&range) as u8)
            .collect();

        raw_data.extend_from_slice(&test_data);

        // Get the offset of the block
        let block_offset = mm2s_offset + block_num * mm2s_block_stride;

        // Need the memory fences here to prevent the cpu from reordering
        // the non volatile reads/writes around volatile reads/writes
        fence(Ordering::SeqCst);

        unsafe {
            // Copy the test data into the source memory
            copy_nonoverlapping(
                test_data.as_ptr(),
                mm2s_dma_data.offset(block_offset as isize),
                *mm2s_block_size);
        }

        fence(Ordering::SeqCst);
    }

    // Create a vector to take the expected destination data
    let mut expected_s2mm_mem = vec!(0u8; s2mm_dma_data_size);

    for n in 0..*s2mm_n_blocks {
        // Work out the block offset in destination memory
        let block_offset = s2mm_offset + n*s2mm_block_stride;
        // Set up the expected destination data
        expected_s2mm_mem[block_offset..(block_offset + s2mm_block_size)]
            .copy_from_slice(&raw_data[n*s2mm_block_size..(n+1)*s2mm_block_size]);
    }

    expected_s2mm_mem
}

fn trigger_wait_and_check_sg (
    mm2s_device: &mut scatter_gather::Device,
    s2mm_device: &mut scatter_gather::Device,
    mm2s_operation: &Operation,
    s2mm_operation: &Operation,
    expected_s2mm_mem: &Vec<u8>,
    expected_mm2s_nbytes_transferred: &usize,
    expected_s2mm_nbytes_transferred: &usize,
    s2mm_first: bool) -> () {

    // Get pointers to and the size of the s2mm device memory
    let (s2mm_dma_data, s2mm_dma_data_size) = s2mm_device.get_memory();

    // Trigger the dma transfers
    if s2mm_first {
        s2mm_device.do_dma(&s2mm_operation).unwrap();
        mm2s_device.do_dma(&mm2s_operation).unwrap();
    }
    else {
        mm2s_device.do_dma(&mm2s_operation).unwrap();
        s2mm_device.do_dma(&s2mm_operation).unwrap();
    }

    let timeout = time::Duration::from_millis(1000);

    // Wait for DMA transfers to complete
    mm2s_device.wait_transfer_complete(&timeout).unwrap();
    s2mm_device.wait_transfer_complete(&timeout).unwrap();

    // Check and refresh the operation
    let mm2s_nbytes_transferred =
        mm2s_device.check_and_refresh_operation(&mm2s_operation).unwrap();
    let s2mm_nbytes_transferred =
        s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();

    // Check that the system returns the expected nbytes transferred
    assert!(*expected_mm2s_nbytes_transferred == mm2s_nbytes_transferred);
    assert!(*expected_s2mm_nbytes_transferred == s2mm_nbytes_transferred);

    // Create a vector into which we can copy the data out of the
    // destination memory
    let mut s2mm_mem = vec!(0u8; s2mm_dma_data_size);

    // Need the memory fences here to prevent the cpu from reordering
    // the non volatile reads/writes around volatile reads/writes
    fence(Ordering::SeqCst);

    unsafe {
        // Read the destination memory.
        copy_nonoverlapping(
            s2mm_dma_data.offset(0 as isize),
            s2mm_mem.as_mut_ptr(), s2mm_dma_data_size);
    }

    fence(Ordering::SeqCst);

    // Check the data
    assert!(s2mm_mem == *expected_s2mm_mem);

    ()
}

/// The system should be able to transfer data from MM2S memory to S2MM
/// memory via the PL using scatter gather DMA operations, where the
/// arguments are defined:
///
///     block_size
///     ----------
///     Size (in bytes) of the blocks of data to be transferred. A block is
///     is the contiguous memory to be read from or written to by the DMA
///     transfer.
///
///     * 8 bytes <= block size < 2**23 - 1 (number of bytes a single descriptor can transfer)
///     * block size is a multiple of 8
///
///     Note: The combination of these two means max block size is 2**23 - 8
///
///     block_stride
///     ------------
///     Difference in offsets between the start of one block and the start of
///     the subsequent block.
///
///     * block size <= block stride < Size of device.dma_data memory
///     * block stride is a multiple of 8
///
///     n_blocks
///     --------
///     The number of blocks to be read from or written to by the DMA
///     transfer.
///
///     * n_blocks <= Size of device.dma_data memory/block_stride
///
///     offset
///     ------
///     The offset of the first block.
///
///     * 0 <= offset < Size of device.dma_data memory
///     * offset is a multiple of 8
///
/// The system should be able to transfer data from any source location in
/// the MM2S memory to any destination location in the S2MM memory.
///
/// The system should be able to do multiple back to back DMA transfers.
#[test]
#[serial]
fn test_sg_dma_transfers() {

    let s2mm_device_path: PathBuf =
        ["/dev", "axi_dma_sg_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf =
        ["/dev", "axi_dma_sg_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        scatter_gather::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device =
        scatter_gather::Device::new(&mm2s_device_path).unwrap();

    // Get a pointer to and the size of the memory for each DMA device
    let (_s2mm_dma_data, s2mm_dma_data_size) = s2mm_device.get_memory();
    let (_mm2s_dma_data, mm2s_dma_data_size) = mm2s_device.get_memory();

    assert!(s2mm_dma_data_size == mm2s_dma_data_size);

    let max_mm2s_blocks = mm2s_device.n_available_descriptors().unwrap();
    let max_s2mm_blocks = s2mm_device.n_available_descriptors().unwrap();

    assert!(max_mm2s_blocks == max_s2mm_blocks);

    let mut rng = rand::thread_rng();

    // Generate a random value for block stride
    let rand_stride = 8*rng.gen_range(1, (1 << 18)/8);

    // Create argument combinations to test:
    //     Smallest permissible block size
    //     Largest permissible block size
    //     Block stride equals block size
    //     Largest permissible block stride
    //     Smallest permissible n_blocks (1)
    //     Largest permissible n_blocks (2048)
    //     Lowest permissible offset
    //     Highest permissible offset
    let block_sizes = vec![8, (1<<23)-8, rand_stride, 8, 8, 8];
    let block_strides = vec![8, (1<<23)-8, rand_stride, (1<<23)-8, 8, 8];
    let all_n_blocks = vec![1, 1, 2, 2, max_mm2s_blocks, 1];
    let offsets = vec![0, 0, 0, 0, 0, mm2s_dma_data_size-8];

    for n in 0..block_sizes.len() {

        let block_size = block_sizes[n];
        let block_stride = block_strides[n];
        let n_blocks = all_n_blocks[n];
        let offset = offsets[n];

        // Calculate the expected number of bytes transferred
        let expected_nbytes_transferred = block_size*n_blocks;

        // Write random data into the relevant appropriate locations in the
        // MM2S memory.
        let expected_s2mm_mem =  gen_and_write_random_data(
            &mm2s_device, &s2mm_device, &block_size, &block_stride, &n_blocks,
            &offset, &block_size, &block_stride, &n_blocks, &offset,);

        // Set up the transfers into and out of the PL
        let mm2s_operation = mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        let s2mm_operation = s2mm_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        let s2mm_first = false;

        trigger_wait_and_check_sg(
            &mut mm2s_device, &mut s2mm_device, &mm2s_operation,
            &s2mm_operation, &expected_s2mm_mem,
            &expected_nbytes_transferred,
            &expected_nbytes_transferred, s2mm_first);
    }
}

/// As long as the argument rules are obeyed, the system should be able to
/// perform any combination of arguments (exluding n_blocks and block_size)
/// for both MM2S and S2MM transfers. For the loopback PL that these tests
/// require, n_blocks and block_size must be equal for both MM2S and S2MM
/// transfers.
///
/// The `check_and_refresh_operation` function should return the number of
/// bytes that have been transferred.
///
/// The system should be able to set up the MM2S operation first and then the
/// S2MM operation or vice versa.
#[test]
#[serial]
fn test_sg_random_dma_transfers() {

    let s2mm_device_path: PathBuf =
        ["/dev", "axi_dma_sg_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf =
        ["/dev", "axi_dma_sg_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        scatter_gather::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device =
        scatter_gather::Device::new(&mm2s_device_path).unwrap();

    // Get pointers to and the size of the S2MM device memory
    let (s2mm_dma_data, s2mm_dma_data_size) = s2mm_device.get_memory();

    // Create a vector to overwrite the destination before beginning the
    // transfer
    let destination_init_data = vec!(0u8; s2mm_dma_data_size);

    // Need the memory fences here to prevent the cpu from reordering
    // the non volatile reads/writes around volatile reads/writes
    fence(Ordering::SeqCst);
    unsafe {
        // Set up the destination memory with known values so we know the
        // state of the memory before the transfer
        copy_nonoverlapping(
            destination_init_data.as_ptr(),
            s2mm_dma_data.offset(0 as isize), s2mm_dma_data_size);
    }
    fence(Ordering::SeqCst);

    // Do four random transfers
    for _n in 0..4 {

        let (n_blocks, block_size, mm2s_block_stride, mm2s_offset,
             s2mm_block_stride, s2mm_offset, expected_destination_mem) =
            setup_random_sg_transfers(&mm2s_device, &s2mm_device);

        // Calculate the expected number of bytes transferred
        let expected_mm2s_nbytes_transferred = block_size*n_blocks;
        let expected_s2mm_nbytes_transferred = block_size*n_blocks;

        // Set up the transfers into and out of the PL
        let mm2s_operation = mm2s_device.new_regular_blocks_operation(
            &block_size, &mm2s_block_stride, &n_blocks,
            &mm2s_offset).unwrap();
        let s2mm_operation = s2mm_device.new_regular_blocks_operation(
            &block_size, &s2mm_block_stride, &n_blocks,
            &s2mm_offset).unwrap();

        // Randomly set s2mm to trigger first
        let s2mm_first: bool = rand::random();

        trigger_wait_and_check_sg(
            &mut mm2s_device, &mut s2mm_device, &mm2s_operation,
            &s2mm_operation, &expected_destination_mem,
            &expected_mm2s_nbytes_transferred,
            &expected_s2mm_nbytes_transferred, s2mm_first);
    }
}

/// It should be possible to create multiple operations on a single device.
/// It should be possible to run these operations in any order.
///
/// The maximum number of operations it is possible to create on a device is
/// the number of descriptor chunks on the device.
#[test]
#[serial]
fn test_sg_multiple_operations() {

    let s2mm_device_path: PathBuf =
        ["/dev", "axi_dma_sg_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf =
        ["/dev", "axi_dma_sg_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        scatter_gather::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device =
        scatter_gather::Device::new(&mm2s_device_path).unwrap();

    // Get the number of descriptor chunks on the devices. This gives us the
    // maximum number of operations we can create.
    let s2mm_n_desc_chunks =
        s2mm_device.n_available_descriptor_chunks().unwrap();
    let mm2s_n_desc_chunks =
        mm2s_device.n_available_descriptor_chunks().unwrap();

    assert!(s2mm_n_desc_chunks == mm2s_n_desc_chunks);

    let max_n_operations = s2mm_n_desc_chunks;

    // Get pointers to and the size of the S2MM device memory
    let (_s2mm_dma_data, s2mm_dma_data_size) = s2mm_device.get_memory();
    // Get pointers to and the size of the MM2S device memory
    let (_mm2s_dma_data, mm2s_dma_data_size) = mm2s_device.get_memory();

    assert!(s2mm_dma_data_size == mm2s_dma_data_size);

    let dma_data_size = s2mm_dma_data_size;

    let mut rng = rand::thread_rng();

    let mut block_sizes = Vec::new();
    let mut block_strides = Vec::new();
    let mut all_n_blocks = Vec::new();
    let mut offsets = Vec::new();
    let mut mm2s_operations = Vec::new();
    let mut s2mm_operations = Vec::new();

    // Create up to the maximum number of operations
    for n in 0..max_n_operations {

        // Generate random arguments for the operations
        block_sizes.push(8*rng.gen_range(1, (1 << 16)/8));
        block_strides.push(8*rng.gen_range(block_sizes[n]/8, (1 << 16)/8));
        // Work out how many blocks we can have up to
        let max_n_blocks = std::cmp::min(
            dma_data_size/block_strides[n],
            scatter_gather::descriptors::N_DESCRIPTORS_PER_CHUNK);
        // We only want to use one descriptor chunk per operation so that we
        // can create the max number of operations
        all_n_blocks.push(rng.gen_range(1, max_n_blocks));
        offsets.push(8*rng.gen_range(
                0, (dma_data_size - block_strides[n]*all_n_blocks[n])/8));

        // Set up the transfers into and out of the PL
        mm2s_operations.push(
            mm2s_device.new_regular_blocks_operation(
                &block_sizes[n], &block_strides[n], &all_n_blocks[n],
                &offsets[n]).unwrap());
        s2mm_operations.push(
            s2mm_device.new_regular_blocks_operation(
                &block_sizes[n], &block_strides[n], &all_n_blocks[n],
                &offsets[n]).unwrap());
    }

    // Check that the descriptors have been consumed in the expected way.
    assert!(s2mm_device.n_available_descriptor_chunks().unwrap() == 0);
    assert!(s2mm_device.n_available_descriptors().unwrap() == 0);
    assert!(mm2s_device.n_available_descriptor_chunks().unwrap() == 0);
    assert!(mm2s_device.n_available_descriptors().unwrap() == 0);

    let mut rng = rand::thread_rng();

    // Do eight random transfers
    for _n in 0..8 {

        // Generate a random value for block stride
        let op = rng.gen_range(0, max_n_operations);

        // Write random data into the relevant appropriate locations in the
        // MM2S memory.
        let expected_s2mm_mem =  gen_and_write_random_data(
            &mm2s_device, &s2mm_device, &block_sizes[op], &block_strides[op],
            &all_n_blocks[op], &offsets[op], &block_sizes[op],
            &block_strides[op], &all_n_blocks[op], &offsets[op]);

        // Calculate the expected number of bytes transferred
        let expected_nbytes_transferred = block_sizes[op]*all_n_blocks[op];

        let s2mm_first = false;

        trigger_wait_and_check_sg(
            &mut mm2s_device, &mut s2mm_device, &mm2s_operations[op],
            &s2mm_operations[op], &expected_s2mm_mem,
            &expected_nbytes_transferred,
            &expected_nbytes_transferred, s2mm_first);
    }
}

/// It should be possible to refresh an operation from outside the library.
/// After running `scatter_gather::device::wait_transfer_complete` function
/// `scatter_gather::device::refresh` should be called before attempting any
/// more transfers with the operation which produced the error.
///
/// This is because after a transfer the descriptors are stale and need to be
/// refreshed. If `scatter_gather::device::wait_transfer_complete`
/// returned an error, then the operation descriptors will still be in a stale
/// state so any subsequent transfers with that operation will fail.
#[test]
#[serial]
fn test_sg_refresh_operation() -> Result<(), String> {
    // We can check that the refresh operation works without relying on
    // wait_transfer_complete to return an error. To simulate the situation
    // when the operation is stale, we trigger an operation and pause (without
    // running wait_transfer_complete) long enough for the transfer to
    // complete. We then try to trigger another transfer with the same
    // operation. This attempt should return a `DmaError::SGInternal` error.
    // We then refresh the operation and trigger the transfer again. This time
    // it should succeed.

    let s2mm_device_path: PathBuf =
        ["/dev", "axi_dma_sg_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf =
        ["/dev", "axi_dma_sg_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        scatter_gather::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device =
        scatter_gather::Device::new(&mm2s_device_path).unwrap();

    let mut rng = rand::thread_rng();

    // Generate random arguments for the operations
    let block_size = 8*rng.gen_range(1, (1 << 10)/8);
    let block_stride = 8*rng.gen_range(block_size/8, (1 << 10)/8);
    let n_blocks = rng.gen_range(1, 10);
    let offset = 0;

    // Set up the transfers into and out of the PL
    let mm2s_operation =
        mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();
    let s2mm_operation =
        s2mm_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

    // Write random data into the relevant appropriate locations in the
    // MM2S memory.
    let _expected_s2mm_mem =  gen_and_write_random_data(
        &mm2s_device, &s2mm_device, &block_size, &block_stride, &n_blocks,
        &offset, &block_size, &block_stride, &n_blocks, &offset);

    // Trigger the dma transfers
    mm2s_device.do_dma(&mm2s_operation).unwrap();
    s2mm_device.do_dma(&s2mm_operation).unwrap();

    // Pause long enough for the DMA transfers to complete
    let pause = time::Duration::from_secs(1);
    thread::sleep(pause);

    // Need to reset the device to clear the transfer in progress flag. This
    // will not refresh the descriptors
    mm2s_device.reset().unwrap();
    s2mm_device.reset().unwrap();

    // The operations should now be in a stale state. Trigger the dma
    // transfers again.
    mm2s_device.do_dma(&mm2s_operation).unwrap();
    s2mm_device.do_dma(&s2mm_operation).unwrap();

    // Need to put another pause in here to make sure the stale operation
    // has time to fail
    thread::sleep(pause);

    let timeout = time::Duration::from_millis(1000);
    // Wait for DMA transfers to complete.
    let mm2s_res = mm2s_device.wait_transfer_complete(&timeout);
    let s2mm_res = s2mm_device.wait_transfer_complete(&timeout);

    //These should both return  `DmaError::SGInternal` errors
    match mm2s_res {
        Err(DmaError::SGInternal) => (),
        _ => panic!("Did not get DmaError::SGInternal error."),
        }
    match s2mm_res {
        Err(DmaError::SGInternal) => (),
        _ => panic!("Did not get DmaError::SGInternal error."),
        }

    // Need to reset the device to clear the error
    mm2s_device.reset().unwrap();
    s2mm_device.reset().unwrap();

    mm2s_device.check_and_refresh_operation(&mm2s_operation).unwrap();
    s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();

    // The operations should now be refreshed. Trigger the dma transfers
    // again.
    mm2s_device.do_dma(&mm2s_operation).unwrap();
    s2mm_device.do_dma(&s2mm_operation).unwrap();

    // Wait for DMA transfers to complete
    mm2s_device.wait_transfer_complete(&timeout).unwrap();
    s2mm_device.wait_transfer_complete(&timeout).unwrap();

    // Check and refresh the operation
    let mm2s_nbytes_transferred =
        mm2s_device.check_and_refresh_operation(&mm2s_operation).unwrap();
    let s2mm_nbytes_transferred =
        s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();

    // Check the operation ran successfully
    assert!(mm2s_nbytes_transferred == block_size*n_blocks);
    assert!(s2mm_nbytes_transferred == block_size*n_blocks);
}

/// The system should return `DmaError::TransferInProgress` if the user calls
/// `do_dma` multiple times without calling
/// `wait_transfer_complete`. In this case it should be possible to return to
/// a known working state by calling `reset`.
#[test]
#[serial]
fn test_transfer_in_progress() -> Result<(), String> {
    // This test will call do_dma_direct twice without calling
    // wait_transfer_complete to check that the device returns a
    // TransferInProgress error. It will then call reset. It will then perform
    // another transfer to check that the device has returned to a working
    // state

    let s2mm_device_path: PathBuf =
        ["/dev", "axi_dma_sg_s2mm"].iter().collect();
    let mm2s_device_path: PathBuf =
        ["/dev", "axi_dma_sg_mm2s"].iter().collect();

    // Create the devices. In the loopback PL the MM2S DMA engine stream
    // output is connected to the S2MM DMA engine stream input.
    let mut s2mm_device =
        scatter_gather::Device::new(&s2mm_device_path).unwrap();
    let mut mm2s_device =
        scatter_gather::Device::new(&mm2s_device_path).unwrap();

    let mut rng = rand::thread_rng();

    // Generate random arguments for the operations
    let block_size = 8*rng.gen_range(1, (1 << 10)/8);
    let block_stride = 8*rng.gen_range(block_size/8, (1 << 10)/8);
    let n_blocks = rng.gen_range(1, 10);
    let offset = 0;

    // Set up the transfers into and out of the PL
    let mm2s_operation =
        mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();
    let s2mm_operation =
        s2mm_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

    // Write random data into the relevant appropriate locations in the
    // MM2S memory.
    let _expected_s2mm_mem =  gen_and_write_random_data(
        &mm2s_device, &s2mm_device, &block_size, &block_stride, &n_blocks,
        &offset, &block_size, &block_stride, &n_blocks, &offset);

    // Trigger the dma transfers
    mm2s_device.do_dma(&mm2s_operation).unwrap();
    s2mm_device.do_dma(&s2mm_operation).unwrap();

    // Check that the second do dma call gets the transfer in progress error
    match mm2s_device.do_dma(&mm2s_operation) {
        Err(DmaError::TransferInProgress) => (),
        _ => panic!("Did not get DmaError::TransferInProgress error."),
    }
    match s2mm_device.do_dma(&s2mm_operation) {
        Err(DmaError::TransferInProgress) => (),
        _ => panic!("Did not get DmaError::TransferInProgress error."),
    }

    // Need to reset the device to clear the transfer_in_progress
    mm2s_device.reset().unwrap();
    s2mm_device.reset().unwrap();

    // Refresh the operation.
    mm2s_device.check_and_refresh_operation(&mm2s_operation).unwrap();
    s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();

    // The device should now be ready to perform a transfer. Trigger the dma
    // transfers again.
    mm2s_device.do_dma(&mm2s_operation).unwrap();
    s2mm_device.do_dma(&s2mm_operation).unwrap();

    let timeout = time::Duration::from_millis(1000);

    // Wait for DMA transfers to complete
    mm2s_device.wait_transfer_complete(&timeout).unwrap();
    s2mm_device.wait_transfer_complete(&timeout).unwrap();

    // Wait for DMA transfers to complete
    let mm2s_nbytes_transferred =
        mm2s_device.check_and_refresh_operation(&mm2s_operation).unwrap();
    let s2mm_nbytes_transferred =
        s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();

    // Check the operation ran successfully
    assert!(mm2s_nbytes_transferred == block_size*n_blocks);
    assert!(s2mm_nbytes_transferred == block_size*n_blocks);
}
