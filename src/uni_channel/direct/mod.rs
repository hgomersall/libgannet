use std::{
    fs::File,
    fs::OpenOptions,
    path::PathBuf,
    time,
};

use memmap::{
    MmapMut,
};

use crate::{
    check_multi_channel_device,
    DeviceSpecifics,
    errors::DmaError,
    enable_interrupts,
    get_and_consume_interrupts,
    read_reg,
    reset,
    setup_cfg_and_data_mem,
    TransferType,
    uni_channel::check_error,
    uni_channel::clear_all_interrupts,
    uni_channel::get_transfer_type,
    uni_channel::halted,
    uni_channel::idle,
    uni_channel::print_status,
    uni_channel::registers::ControlRegister,
    uni_channel::registers::Registers,
    uni_channel::scatter_gather_enabled,
    uni_channel::write_reset_bit,
    uni_channel::write_stop_bit,
    write_reg,
    wait_for_halted,
    wait_for_idle,
};

/// The Device structure encapsulates all the information required for the
/// system to perform direct DMA operations. There are methods on the Device
/// structure which enable these operations.
#[derive(Debug)]
pub struct Device {
    cfg: *mut u32,
    cfg_mem: MmapMut,
    dma_data: *mut u8,
    dma_data_mem: MmapMut,
    dma_data_size: usize,
    dma_data_phys_addr: usize,
    transfer_type: TransferType,
    file: File,
    transfer_in_progress: bool,
}

impl Drop for Device {
    fn drop(&mut self) {
        // Reset the underlying device on drop. Ignore any errors as the
        // device is being dropped anyway.
        let _ = self.reset();
    }
}

// Implement the DeviceSpecifics trait
impl DeviceSpecifics for Device {

    fn write_reset_bit(&self) {
        write_reset_bit(&self.cfg, &self.transfer_type);
    }

    fn halted(&self) -> bool {
        halted(&self.cfg, &self.transfer_type)
    }

    fn idle(&self) -> bool {
        idle(&self.cfg, &self.transfer_type)
    }

    fn check_error(&self) -> Result<(), DmaError> {
        check_error(&self.cfg, &self.transfer_type)
    }

    fn get_and_consume_interrupts(&mut self, timeout: &time::Duration)
        -> Result<usize, DmaError> {
            get_and_consume_interrupts(&mut self.file, timeout)
        }
}

impl Device {

    /// A function to create the device and get it ready for use.
    ///
    /// This function should be run first before attempting any DMA transfers.
    ///
    /// In the case of failure this function will return the causing error.
    pub fn new(device_path: &PathBuf) -> Result<Device, DmaError> {

        // Initialise the device
        let mut device = Device::init(&device_path)?;

        // Reset the device
        device.reset()?;

        Ok(device)
    }

    /// A function to initiate the direct DMA device.
    ///
    /// Given a system file, this function extracts all the required
    /// information and returns a direct DMA device.
    fn init(device_path: &PathBuf) -> Result<Device, DmaError> {

        // Open the device file
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(&device_path)?;

        let (cfg_mem, dma_data_mem, dma_data_size, dma_data_phys_addr) =
            setup_cfg_and_data_mem(device_path, &file)?;

        // Get a raw pointer to the configuration memory
        let cfg = cfg_mem.as_ptr() as *mut u32;

        if check_multi_channel_device(&cfg)? {
            // Error if the device if a multi channel device
            return Err(DmaError::UnsupportedMultiChannel);
        }

        if scatter_gather_enabled(&cfg)? {
            // Error if the device is scatter gather enabled
            return Err(DmaError::UnsupportedScatterGather);
        }

        let transfer_type = get_transfer_type(&cfg)?;

        // Get a raw pointers to the dma_data memory
        let dma_data = dma_data_mem.as_ptr() as *mut u8;

        let transfer_in_progress = false;

        Ok(Device {
            cfg,
            cfg_mem,
            dma_data,
            dma_data_mem,
            dma_data_size,
            dma_data_phys_addr,
            transfer_type,
            file,
            transfer_in_progress,
        })
    }

    /// Perform a reset on the device.
    ///
    /// This function will reset the DMA device, even if it is waiting for
    /// completion of a transfer.
    ///
    /// It will return `Ok` if successful or `DmaError::ResetFailed` if it
    /// unsuccessful.
    pub fn reset(&mut self) -> Result<(), DmaError> {

        self.transfer_in_progress = false;

        reset(self)
    }

    /// A function to do a DMA transfer.
    ///
    /// It checks that a transfer is not in progress. Then checks that the
    /// arguments passed to it are valid and returns errors if not.
    ///
    /// The possible returned errors are DmaError::TransferInProgress,
    /// DmaError::InvalidSize, DmaError::InvalidOffset or
    /// DmaError::MemoryOverflow.
    ///
    /// If successful this function returns Ok.
    ///
    /// The function writes to the DMA cfg registers to set up the transfer.
    /// Note that the function writes to the length register last. This is
    /// important as it writing to this register which triggers the transfer.
    /// All other cfg registers must have been written to beforehand.
    ///
    /// Note: offset should be given in bytes.
    ///
    /// Note: The Registers::Mm2sLength and Registers::S2mmLength registers
    /// can take a length of up to 2**26. However in Vivado where we create
    /// the DMA engine, the maximum length it can handle is 2**23. It is
    /// possible to set the maximum length lower than 2**23 in Vivado in which
    /// case this library will not detect the issue. It is up to the user to
    /// ensure the nbytes passed to this function will work with the DMA
    /// engine in their PL.
    pub fn do_dma(
        &mut self, nbytes: &usize, offset: &usize) -> Result<(), DmaError> {

        // Check that the device is not currently doing a transfer. If it is
        // then error
        if self.transfer_in_progress {
            return Err(DmaError::TransferInProgress);
        }

        // Check that the transfer size is valid. 1<<23 is used here as this
        // is the maximum length the DMA engine can be set to take in Vivado.
        if *nbytes < 8 || *nbytes >= 1<<23 || *nbytes % 8 != 0 {
            return Err(DmaError::InvalidSize);
        }

        // Check that the offset is valid. Offset cannot be less than 0 due to
        // type limits.
        if *offset >= self.dma_data_size || offset % 8 != 0 {
            return Err(DmaError::InvalidOffset);
        }

        // Check that the combination of offset and nbytes is valid
        if *offset + *nbytes > self.dma_data_size {
            return Err(DmaError::MemoryOverflow);
        }

        // The DMA device has two 32 bit registers to set the destination or
        // source address for the transfer. One takes the most significant
        // word of the address (address_msw), the other the least significant
        // word (address_lsw). Note: this means the DMA device can only handle
        // address ranges up to 2**64.
        //
        // If we are on a 64 bit architecture casting to u32 takes the bottom
        // 32 bits. If the architecture is 32 bits or smaller then casting to
        // u32 will take the whole number.
        let address_lsw: u32 = (self.dma_data_phys_addr + offset) as u32;
        // If it is a 64 bit architecture we need to get the most significant
        // word of the address. To get this we cast to u64, shift it down by
        // 32 bits then cast that to u32. If the architecture is 32 bit or
        // smaller than the msw of the address will just be zero.
        let address_msw: u32 = (
            (self.dma_data_phys_addr + offset) as u64 >> 32) as u32;

        enable_interrupts(&mut self.file)?;

        // Determine which ctrl, address and length registers need to be
        // used depending on the transfer type.
        let (ctrl_reg, address_lsw_reg, address_msw_reg, length_reg) = match
            self.transfer_type {
                TransferType::MM2S => (
                    Registers::Mm2sControlRegister,
                    Registers::Mm2sStartAddressLsw,
                    Registers::Mm2sStartAddressMsw,
                    Registers::Mm2sLength,),
                TransferType::S2MM => (
                    Registers::S2mmControlRegister,
                    Registers::S2mmDestAddressLsw,
                    Registers::S2mmDestAddressMsw,
                    Registers::S2mmLength,),
            };

        // Set transfer in progress on the device
        self.transfer_in_progress = true;

        // SET THE DMA CONFIG REGISTER FOR THE RUN
        // Set:
        //     run bit
        //     interrupt on complete bit
        //     interrupt on error bit
        // All other config bits are set to 0, except IRQThreshold, which
        // implicitly acquires the default value of 1.
        write_reg(
            &self.cfg, &ctrl_reg.offset(),
            ControlRegister::RunStop.bitmask() |
            ControlRegister::IocIrqEn.bitmask() |
            ControlRegister::ErrIrqEn.bitmask());

        // Set the source/destination address of the transfer
        write_reg(&self.cfg, &address_lsw_reg.offset(), address_lsw);
        write_reg(&self.cfg, &address_msw_reg.offset(), address_msw);

        // BEGIN THE RUN
        // Write the number of bytes to transfer. This initiates the transfer.
        // We can safely cast to u32 here as we have already checked that
        // nbytes is valid.
        write_reg(&self.cfg, &length_reg.offset(), *nbytes as u32);

        Ok(())
    }

    /// A function to wait for the completion of the transfer. First it waits
    /// for an interrupt indicating completion of the DMA then it keeps
    /// checking the device status register until the transfer finishes. It
    /// then clears the interrupt and stops the device. It will block for up
    /// to the timeout period. If the transfer does not complete by then it
    /// will return. In this case the device should be reset before another
    /// transfer is attempted.
    ///
    /// If successful this function will return the number of bytes
    /// transferred.
    ///
    /// Possible errors include: DmaError::Slave, DmaError::Decode,
    /// DmaError::TimedOut.
    pub fn wait_transfer_complete(
        &mut self, timeout: &time::Duration) -> Result<u32, DmaError> {

        // Determine which length register needs to be used depending on the
        // transfer type.
        let length_reg = match self.transfer_type {
            TransferType::MM2S => Registers::Mm2sLength,
            TransferType::S2MM => Registers::S2mmLength,
        };

        // Get the time now
        let get_and_consume_interrupts_time_check = time::Instant::now();

        // An interrupt means that the DMA has completed but (in the case off
        // an MM2S transfer) the data has not necessarily been streamed out of
        // the DMA engine. The DMA engine buffers some data so the DMA can
        // complete without the data streaming out of the DMA engine. The
        // device sets the idle bit high when the data has been streamed out.
        // This is checked below.
        if self.get_and_consume_interrupts(&timeout)? == 0 {
            // Poll timed out
            return Err(DmaError::TimedOut);
        }

        // Work out how much of the timeout we have remaining after some was
        // used waiting for the interrupt.
        let timeout_used = get_and_consume_interrupts_time_check.elapsed();
        let timeout_remaining =
            if timeout_used > *timeout {time::Duration::from_secs(0)}
            else {*timeout - timeout_used};

        // Check the error status of the DMA transfer.
        self.check_error()?;

        // If we are doing an MM2S transfer we wait and check if the
        // device is idle. We need to do this as the DMA engine sends an
        // interrupt when the DMA is complete which is not necessarily
        // when the data has streamed out of the DMA engine (MM2S
        // transfers). When the device has returned to idle, it has
        // definitely finished streaming the data out.
        // S2MM transfers do not suffer from this issue as the DMA
        // finishes after the data has finished streaming in.
        match self.transfer_type {
            TransferType::MM2S => wait_for_idle(self, &timeout_remaining)?,
            TransferType::S2MM => (),
        }

        // Write to the status register to clear the interrupt.
        clear_all_interrupts(&self.cfg, &self.transfer_type);

        // Set control register to 0. This is to set the run/stop bit low
        // which halts the device.
        write_stop_bit(&self.cfg, &self.transfer_type);

        // Wait for the status register to show the device is halted. At that
        // point, we can refresh the descriptor chain.
        wait_for_halted(self, &time::Duration::from_millis(100))?;

        // Read the number of bytes transferred by the DMA device
        let nbytes: u32 = read_reg(&self.cfg, &length_reg.offset());

        // Transfer has finished
        self.transfer_in_progress = false;

        Ok(nbytes)
    }

    /// A function which returns a pointer to the device memory.
    ///
    /// This function returns a tuple which includes a pointer to the device
    /// memory allowing the calling program to directly write the data for the
    /// DMA transfer or directly read the data from the DMA transfer. Along
    /// with the size of the memory space (dma_data, dma_data_size).
    pub fn get_memory(&self) -> (*mut u8, usize) {
        // We are happy to return this pointer to the calling function because
        // the underlying memory has been permanently allocated to the device
        // by the kernel (This happens in the driver).
        (self.dma_data, self.dma_data_size)
    }

    /// This function prints the status of the device in human readable form.
    pub fn print_status(&self) {
        print_status(&self.cfg, &self.transfer_type);
    }
}

#[cfg(test)]
mod tests {

    use std::{
        path::PathBuf,
        time,
    };
    use rand::Rng;
    use serial_test_derive::serial;

    use super::{
        Device,
        DmaError,
    };

    /// The `dma::direct::device::new` function should return an
    /// UnsupportedScatterGather error if the underlying device is a scatter
    /// gather DMA device.
    #[test]
    #[serial]
    fn test_unsupported_scatter_gather() {
        let device_path: PathBuf = ["/dev", "axi_dma_sg_mm2s"].iter()
            .collect();

        let device = Device::new(&device_path);

        match device {
            Err(DmaError::UnsupportedScatterGather) => (),
            _ => panic!(
                    "Did not get DmaError::UnsupportedScatterGather error."),
        }
    }

    /// The `dma::direct::device::new` function should return an
    /// UnsupportedCapability error if the underlying device is capable of
    /// performing both MM2S and S2MM.
    #[test]
    #[serial]
    fn test_unsupported_capability() {

        let device_path: PathBuf = ["/dev", "axi_dma_combined"].iter()
            .collect();

        let device = Device::new(&device_path);

        match device {
            Err(DmaError::UnsupportedCapability) => (),
            _ => panic!("Did not get DmaError::UnsupportedCapability error."),
        }
    }

    /// The `dma::direct::device::new` function should return an
    /// UnsupportedMultiChannel error if the underlying device is a multi
    /// channel device.
    #[test]
    #[serial]
    fn test_unsupported_multi_channel() {

        let device_path: PathBuf = ["/dev", "axi_dma_mc_mm2s"].iter()
            .collect();

        let device = Device::new(&device_path);

        match device {
            Err(DmaError::UnsupportedMultiChannel) => (),
            _ => panic!(
                    "Did not get DmaError::UnsupportedMultiChannel error."),
        }
    }

    /// The `dma::direct::device::wait_transfer_complete` function should
    /// return `DmaError::TimedOut` if it does not receive an interrupt within
    /// the `timeout` period
    #[test]
    #[serial]
    fn test_wait_transfer_complete_timeout() {
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

        let mut device = Device::new(&device_path).unwrap();

        let timeout = time::Duration::from_millis(100);

        // We have not triggered a transfer so wait_transfer_complete should
        // return DmaError::TimedOut
        let response = device.wait_transfer_complete(&timeout);

        match response {
            Err(DmaError::TimedOut) => (),
            _ => panic!("Did not get DmaError::TimedOut error."),
        }
    }

    /// The `dma::direct::device::do_dma` function should check that
    /// the device is not already performing a transfer. If it is then it
    /// should return `DmaError::TransferInProgress`.
    #[test]
    #[serial]
    fn test_transfer_in_progress() {

        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let nbytes: usize = 128;
        let offset: usize = 0;

        // Trigger a dma
        device.do_dma(&nbytes, &offset).unwrap();

        // Try to trigger a second DMA transfer without calling
        // wait_transfer_complete or reset. This should return
        // TransferInProgress
        let do_dma_res = device.do_dma(&nbytes, &offset);

        match do_dma_res {
            Err(DmaError::TransferInProgress) => (),
            _ => panic!("Did not get DmaError::TransferInProgress error."),
        }
    }

    /// The `dma::direct::device::do_dma` function should check that
    /// the `nbytes` argument is in the range 8 -> 2**23 and that it is a
    /// multiple of 8. If either of these is not true then it should return a
    /// `DmaError::InvalidSize` error.
    #[test]
    #[serial]
    fn test_invalid_size() {

        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let nbytes: usize = loop {
            // Generate a random nbytes
            let mut rng = rand::thread_rng();
            let nbytes: usize = rng.gen();

            if nbytes < 8 || nbytes >= 1<<23 || nbytes % 8 != 0 {
                // Check that nbytes is invalid, if it is then return it
                break nbytes;
            }
        };

        let offset: usize = 0;

        let do_dma_res = device.do_dma(&nbytes, &offset);

        match do_dma_res {
            Err(DmaError::InvalidSize) => (),
            _ => panic!("Did not get DmaError::InvalidSize error."),
        }
    }

    /// The `dma::direct::device::do_dma` function should check that
    /// the `offset` argument is in the range 0 -> `dma_data_size` and that it
    /// is a multiple of 8. If either of these is not true then it should
    /// return a `DmaError::InvalidOffset` error.
    #[test]
    #[serial]
    fn test_invalid_offset() {

        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();

        let mut device = Device::new(&device_path).unwrap();

        let nbytes: usize = 16;

        let offset: usize = loop {
            // Generate a random offset
            let mut rng = rand::thread_rng();
            let offset: usize = rng.gen();

            if offset >= device.dma_data_size || offset % 8 != 0 {
                // Check that offset is invalid, if it is then return it
                break offset;
            }
        };

        let do_dma_res = device.do_dma(&nbytes, &offset);

        match do_dma_res {
            Err(DmaError::InvalidOffset) => (),
            _ => panic!("Did not get DmaError::InvalidOffset error."),
        }
    }

    /// The `dma::direct::device::do_dma` function should check that
    /// the `nbytes` and `offset` arguments will not sum to greater than
    /// `dma_data_size`. If they do then it would cause a memory overflow. The
    /// `dma::device::do_dma` function should prevent this by returning
    /// a `DmaError::MemoryOverflow` error.
    #[test]
    #[serial]
    fn test_memory_overflow() {

        // Create the device
        let device_path: PathBuf = ["/dev", "axi_dma_mm2s"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let (nbytes, offset) = loop {
            // Generate a random nbytes
            let mut rng = rand::thread_rng();
            // Create a valid nbytes.
            let nbytes: usize = 8*rng.gen_range(1, (1<<23)/8);
            // Create a valid offset that will cause the memory to overflow
            let offset: usize = 8*rng.gen_range(
                (device.dma_data_size-nbytes)/8, device.dma_data_size/8);

            if (offset + nbytes) > device.dma_data_size {
                // Check that nbytes and offset would result in a memory
                // overflow, if it would then return it
                break (nbytes, offset);
            }
        };

        let do_dma_res = device.do_dma(&nbytes, &offset);

        match do_dma_res {
            Err(DmaError::MemoryOverflow) => (),
            _ => panic!("Did not get DmaError::MemoryOverflow error."),
        }
    }
}
