pub mod errors;
pub mod uni_channel;
pub mod multi_channel;

use std::{
    cell::RefCell,
    fs,
    fs::File,
    io::Read,
    io::Write,
    os::unix::io::AsRawFd,
    path::Path,
    path::PathBuf,
    ptr::copy_nonoverlapping,
    ptr::read_volatile,
    ptr::write_volatile,
    rc::Rc,
    sync::atomic::fence,
    sync::atomic::Ordering,
    thread,
    time,
};

use memmap::{
    MmapMut,
    MmapOptions,
};

use nix::poll::{
    poll,
    PollFd,
    PollFlags,
};

use math::round::ceil;

use errors::DmaError;

// FIXME we need a ctrl-c handler to reset the device:
// SIGINT, SIGHUP, SIGQUIT

const UIO_PATH: &'static str = "/sys/class/uio";

// These three paths rely on the driver mapping:
// cfg = map0
// dma_data = map1
// dma_descriptors = map2
// If the driver is modified these may change. We check this and will return
// an error if the mappings change.
const CFG_PATH: &'static str = "maps/map0";
const DMA_DATA_PATH: &'static str = "maps/map1";
const DMA_DESCRIPTORS_PATH: &'static str = "maps/map2";

const SIZE_PATH: &'static str = "size";
const ADDR_PATH: &'static str = "addr";
const NAME_PATH: &'static str = "name";

#[derive(Debug)]
pub enum TransferType {
    MM2S,
    S2MM,
}

// Create a trait so that we can pass all device types into functions and be
// certain that the function can call the required methods.
trait DeviceSpecifics {
    fn write_reset_bit(&self);
    fn halted(&self) -> bool;
    fn idle(&self) -> bool;
    fn check_error(&self) -> Result<(), DmaError>;
    fn get_and_consume_interrupts(&mut self, timeout: &time::Duration)
        -> Result<usize, DmaError>;
}

// Create a trait so that we can pass both scatter gather device and multi
// channel devices into functions and be certain that the function can call
// the required methods.
trait SGAndMCDeviceSpecifics {
    fn dma_data_size(&self) -> &usize;
    fn dma_data_phys_addr(&self) -> &usize;
    fn dma_descriptors(&self) -> &*mut u32;
    fn dma_descriptors_phys_addr(&self) -> &usize;
    fn available_descriptor_chunks(&self) -> &Rc<RefCell<Vec<usize>>>;
    fn max_bytes_transfer_per_descriptor(&self) -> &usize;
    fn n_descriptors_per_chunk(&self) -> &usize;
    fn descriptor_stride(&self) -> &usize;
    fn descriptor_size(&self) -> &usize;
    fn start_of_frame_bitmask(&self) -> u32;
    fn end_of_frame_bitmask(&self) -> u32;
    fn desc_field_nxt_desc_lsw_offset(&self) -> usize;
    fn desc_field_nxt_desc_msw_offset(&self) -> usize;
    fn desc_field_buffer_addr_lsw_offset(&self) -> usize;
    fn desc_field_buffer_addr_msw_offset(&self) -> usize;
    fn desc_field_control_offset(&self) -> usize;
    fn descriptor_status(&self, descriptor_offset: usize)
        -> Result<usize, DmaError>;
    fn refresh_descriptor(&self, descriptor_offset: usize);
}

/// The operation structure encapsulates the information required to perform a
/// scatter gather operation (either multi or uni channel). There is an
/// overhead in setting up a scatter gather operation so it can be useful to
/// keep an operation for multiple runs.
#[derive(Debug)]
pub struct Operation {
    device_available_descriptor_chunks: Rc<RefCell<Vec<usize>>>,
    descriptor_chunks: Vec<usize>,
    n_descriptors: usize,
    first_descriptor_phys_addr: u64,
    last_descriptor_phys_addr: u64,
}

impl Operation {

    /// A function to create a new scatter gather operations (either multi
    /// of uni channel). This function should be run before attempting any
    /// scatter gather transfers.
    ///
    /// The device upon which this operation is created should lend the
    /// operation some descriptor chunks. It should also give it a cloned copy
    /// of the available_descriptor_chunks so that the operation can return
    /// the lent descriptor chunks when it is dropped.
    fn new(
        device_available_descriptor_chunks: Rc<RefCell<Vec<usize>>>,
        descriptor_chunks: Vec<usize>, n_descriptors: usize,
        first_descriptor_phys_addr: u64, last_descriptor_phys_addr: u64) ->
        Result<Operation, DmaError> {

            // Create the operation structure.
            Ok(Operation {
                device_available_descriptor_chunks,
                descriptor_chunks,
                n_descriptors,
                first_descriptor_phys_addr,
                last_descriptor_phys_addr,
            })
    }

    /// A function to check that the operation was created on the device which
    /// owns the device_available_descriptor_chunks passed in to this
    /// function.
    fn same_device(
        &self, device_available_descriptor_chunks: &Rc<RefCell<Vec<usize>>>)
        -> bool {

            Rc::ptr_eq(
                &device_available_descriptor_chunks,
                &self.device_available_descriptor_chunks)
    }

    /// A function to return the physical address of the first descriptor
    fn first_descriptor_phys_addr(&self) -> &u64 {
        &self.first_descriptor_phys_addr
    }

    /// A function to return the physical address of the last descriptor
    fn last_descriptor_phys_addr(&self) -> &u64 {
        &self.last_descriptor_phys_addr
    }

    /// A function to return the descriptor chunk offsets of the operation
    fn descriptor_chunk_offsets(&self) -> &Vec<usize> {
        &self.descriptor_chunks
    }

    /// A function to return the number of descriptors on the operation
    fn n_descriptors(&self) -> &usize {
        &self.n_descriptors
    }

    pub fn _print_descriptor_chunks(&self) {
        println!("descriptor_chunks = {:?}", self.descriptor_chunks);
        println!(
            "device_available_descriptor_chunks = {:?}",
            self.device_available_descriptor_chunks);
    }
}

impl Drop for Operation {
    fn drop(&mut self) {

        // Return the operation descriptor chunks to the device
        self.device_available_descriptor_chunks.borrow_mut()
            .append(&mut self.descriptor_chunks);
    }
}

fn reset(device: &mut impl DeviceSpecifics) -> Result<(), DmaError> {

    // Write to the device registers to reset the device
    device.write_reset_bit();

    // Wait for the status register to show the device is halted. At that
    // point, the reset has completed. If wait for halted timedout return
    // DmaError::ResetFailed.
    match wait_for_halted(device, &time::Duration::from_millis(100)) {
        Err(DmaError::HaltTimedOut) => return Err(DmaError::ResetFailed),
        _ => (),
    }

    // The DMA engine could send an interrupt at the same time as we reset
    // it hence creating a race condition. If the interrupt was sent it
    // needs to be consumed so that subsequent operations work correctly.
    // By running get_and_consume_interrupts here we handle this
    // condition.
    device.get_and_consume_interrupts(&time::Duration::from_secs(0))?;

    Ok(())
}

// This function sets up the cfg and data memory for the device.
fn setup_cfg_and_data_mem (device_path: &PathBuf, file: &File) -> Result<(
    MmapMut, MmapMut, usize, usize), DmaError> {

    // Read the symbolic link to get the path of the underlying file
    let uio_name = fs::read_link(device_path)?;

    // Set up the path to the file specifying the size of the configuration
    // register space
    let cfg_size_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(CFG_PATH),
        Path::new(SIZE_PATH)]
            .iter()
            .collect();
    // Read the size of the configuration register space
    let cfg_size = fs::read_to_string(cfg_size_fp)?;
    // Trim the leading 0x and the leading & trailing whitespace from the
    // cfg_size then convert to an integer
    let cfg_size = usize::from_str_radix(
        cfg_size.trim_start_matches("0x").trim(), 16)?;

    // Memory map the configuration register
    let cfg_mem = unsafe {
        MmapOptions::new()
            .offset(0)
            .len(cfg_size)
            .map_mut(&file)?
    };

    // Set up the path to the file specifying the name of the DMA data memory
    // space
    let dma_data_name_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DATA_PATH),
        Path::new(NAME_PATH)]
            .iter()
            .collect();
    // Read the name of underlying memory
    let dma_data_name = fs::read_to_string(dma_data_name_fp)?;
    // Trim trailing whitespace
    let dma_data_name = dma_data_name.trim();
    // Check that the name of the underlying memory is dma_data. This is a
    // sanity check to make sure we have the correct memory. If the driver
    // changes the order of the mappings then this function will return an
    // error
    match dma_data_name.as_ref() {
        "dma_data" => (),
        _ => return Err(DmaError::InvalidMemMap),
    }

    // Set up the path to the file specifying the size of the DMA data
    // memory space
    let dma_data_size_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DATA_PATH),
        Path::new(SIZE_PATH)]
            .iter()
            .collect();
    // Read the size of the DMA_DATA memory space
    let dma_data_size = fs::read_to_string(dma_data_size_fp)?;
    // Trim the leading 0x and the leading & trailing whitespace from the
    // dma_data_size then convert to an integer.
    let dma_data_size = usize::from_str_radix(
        dma_data_size.trim_start_matches("0x").trim(), 16)?;

    // Memory map the DMA_DATA memory
    let dma_data_mem = unsafe {
        MmapOptions::new()
            .offset(page_size::get() as u64)
            .len(dma_data_size)
            .map_mut(&file)?
    };

    // Set up the path to the file specifying the physical address of the
    // DMA_DATA memory space
    let dma_data_phys_addr_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DATA_PATH),
        Path::new(ADDR_PATH)]
            .iter()
            .collect();
    // Read the physical address of the DMA_DATA memory space. This is set up
    // by the kernel and written to a file so we can access it.
    let dma_data_phys_addr = fs::read_to_string(dma_data_phys_addr_fp)?;
    // Trim the leading 0x and the leading & trailing whitespace from the
    // dma_data_phys_addr then convert to an integer.
    let dma_data_phys_addr = usize::from_str_radix(
        dma_data_phys_addr.trim_start_matches("0x").trim(), 16)?;

    Ok((cfg_mem, dma_data_mem, dma_data_size, dma_data_phys_addr))
}

// This function sets up the descriptor memory for the device.
fn setup_descriptors_mem (device_path: &PathBuf, file: &File) -> Result<(
    MmapMut, usize, usize), DmaError> {

    // Read the symbolic link to get the path of the underlying file
    let uio_name = fs::read_link(device_path)?;

    // Set up the path to the file specifying the name of the DMA
    // descriptors memory space
    let dma_descriptors_name_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DESCRIPTORS_PATH),
        Path::new(NAME_PATH)]
            .iter()
            .collect();
    // Read the name of underlying memory
    let dma_descriptors_name =
        fs::read_to_string(dma_descriptors_name_fp)?;
    // Trim trailing whitespace
    let dma_descriptors_name = dma_descriptors_name.trim();
    // Check that the name of the underlying memory is dma_descriptors.
    // This is a sanity check to make sure we have the correct memory. If
    // the driver changes the order of the mappings then this function
    // will return an error
    match dma_descriptors_name.as_ref() {
        "dma_descriptors" => (),
        _ => return Err(DmaError::InvalidMemMap),
    }

    // Set up the path to the file specifying the size of the DMA
    // descriptors memory space
    let dma_descriptors_size_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DESCRIPTORS_PATH),
        Path::new(SIZE_PATH)]
            .iter()
            .collect();
    // Read the size of the DMA descriptors memory space
    let dma_descriptors_size =
        fs::read_to_string(dma_descriptors_size_fp)?;
    // Trim the leading 0x and the leading & trailing whitespace from the
    // dma_descriptors_size then convert to an integer.
    let dma_descriptors_size = usize::from_str_radix(
        dma_descriptors_size.trim_start_matches("0x").trim(), 16)?;

    // Set up the path to the file specifying the physical address of the
    // DMA descriptors memory space
    let dma_descriptors_phys_addr_fp: PathBuf = [
        Path::new(UIO_PATH),
        uio_name.as_path(),
        Path::new(DMA_DESCRIPTORS_PATH),
        Path::new(ADDR_PATH)]
            .iter()
            .collect();
    // Read the physical address of the DMA descriptors memory space. This
    // is set up by the kernel and written to a file so we can access it.
    let dma_descriptors_phys_addr =
        fs::read_to_string(dma_descriptors_phys_addr_fp)?;
    // Trim the leading 0x and the leading & trailing whitespace from the
    // dma_descriptors_phys_addr then convert to an integer.
    let dma_descriptors_phys_addr = usize::from_str_radix(
        dma_descriptors_phys_addr.trim_start_matches("0x").trim(), 16)?;

    // Memory map the descriptors memory space
    let dma_descriptors_mem = unsafe {
        MmapOptions::new()
            .offset((2*page_size::get()) as u64)
            .len(dma_descriptors_size)
            .map_mut(&file)?
    };

    Ok((dma_descriptors_mem, dma_descriptors_size, dma_descriptors_phys_addr))
}

// This function tests the device file to see if an interrupt has been
// received. It will wait up to `timeout_ms` milliseconds for an interrupt.
// If an error is not encountered this function will always return
// `Ok(n_files_interrupted)` where n_files_interrupted represents the number
// of files that have interrupted since the file was last `read`. This
// function can only receive a single file so n_files_interrupted can be a
// maximum of 1. If n_files_interrupted is not zero, the file has interrupted.
//
// This function timed out if n_files_interrupted = 0.
//
// If this function detects any interrupts on the file it will consume them.
fn get_and_consume_interrupts(
    file: &mut File, timeout: &time::Duration) -> Result<usize, DmaError> {

    // Convert timeout from duration to integer
    let timeout_ms = timeout.as_millis() as i32;

    let poll_fd = PollFd::new(file.as_raw_fd(), PollFlags::POLLIN);

    // Check to see if the file is ready for reading.
    let n_files_interrupted = poll(&mut [poll_fd], timeout_ms)?;

    if n_files_interrupted > 0 {
        // Means an interrupt was received and the file is ready for reading
        // so consume the interrupt and return.

        // Read the file to consume any previously received interrupts.
        // Interrupt buffer needs to be big enough to take 4 bytes.
        let mut interrupt_buffer = [0u8; 4];
        file.read(&mut interrupt_buffer)?;
    }

    Ok(n_files_interrupted as usize)
}

// This function waits for the halted bit to be set high. It will wait up to
// timeout. If halted is not set high within timeout it will return a
// DmaError::HaltTimedOut error.
fn wait_for_halted(device: &impl DeviceSpecifics, timeout: &time::Duration)
    -> Result<(), DmaError> {

        // Set the time period for the loop sleeps in microseconds
        let delay_duration = time::Duration::from_micros(1);
        // Get the time now
        let wait_for_halted_time_check = time::Instant::now();

        loop {

            // If the device is halted then break.
            if device.halted() {
                break;
            }

            // Check if we have exceeded the acceptable length of time for
            // halting
            if wait_for_halted_time_check.elapsed() > *timeout {
                // Halting took too long so return an error
                return Err(DmaError::HaltTimedOut);
            }

            // Pause so that we don't waste lots of energy looping
            thread::sleep(delay_duration);
        }

        Ok(())
}

// This function waits for the idle bit to be set high. It will wait up
// to timeout. If idle is not set high within timeout it will return a
// DmaError::TimedOut error.
fn wait_for_idle(device: &mut impl DeviceSpecifics, timeout: &time::Duration)
    -> Result<(), DmaError> {

        // Set the time period for the sleeps in microseconds
        let delay_duration = time::Duration::from_micros(4);
        // Get the time now
        let wait_for_idle_time_check = time::Instant::now();

        // We loop and keep checking if the device is idle.
        loop {

            // Check if the idle bit is set
            if device.idle() {
                break;
            }

            // Check if we have exceeded the acceptable length of time waiting
            // for idle
            if wait_for_idle_time_check.elapsed() > *timeout {
                return Err(DmaError::TimedOut);
            }

            // Pause so that we don't waste lots of energy looping
            thread::sleep(delay_duration);
        }

        Ok(())
}

// This function reads the register at offset in mem and returns the value.
fn read_reg(mem: &*mut u32, offset: &isize) -> u32 {

    // Need the memory fences here to prevent the cpu from reordering
    // the non volatile reads/writes around volatile reads/writes
    fence(Ordering::SeqCst);

    // Read the register
    let reg_val: u32 =
        unsafe {read_volatile(mem.offset(*offset))};

    fence(Ordering::SeqCst);

    reg_val
}

// This function writes val to the register at offset in mem.
fn write_reg(mem: &*mut u32, offset: &isize, val: u32) {

    // Need the memory fences here to prevent the cpu from reordering
    // the non volatile reads/writes around volatile reads/writes
    fence(Ordering::SeqCst);

    // Write to the register
    unsafe {write_volatile(mem.offset(*offset), val)};

    fence(Ordering::SeqCst);
}

// This function enables interrupts in the UIO driver for the device
// represented by file.
fn enable_interrupts(file: &mut File) -> Result<(), DmaError> {
    // Enable interrupts in the uio driver by writing a 1 to the uio
    // file. Note: write_all takes a vector of u8 values but we need
    // to write 32 bits to the file, hence why we have four u8 values
    // in the vector.
    let irq_enable = vec![1u8, 0u8, 0u8, 0u8];
    file.write_all(&irq_enable)?;

    // flush is required to make sure the data is written through the
    // cache to device. Without flush it is possible to get race
    // conditions whereby the interrupt returns before the device file
    // interrupts are enabled.
    file.flush()?;

    Ok(())
}

// This function writes the regular blocks descriptor chain.
//
// To start with it checks that the arguments are valid such that:
//     8 < block size < max_bytes_per_descriptor
//     block_size is a multiple of 8
//
//     offset < dma_data_size
//     offset is a multiple of 8
//
//     block_size < block_stride < dma_data_size
//     block_stride is a multiple of 8
//
//     n_blocks can fit within the memory
//
//     Combination of block_size, block_stride, n_blocks and offset will not
//     cause a memory overflow.
//
// It then writes the descriptors which will cause the DMA engine to perform
// the requested regular blocks operation.
//
// It returns the number of descriptors & descriptor chunks required by the
// operation and the physical addresses of the first and last descriptors in
// the chain in the form:
// (n_descriptors, n_descriptor_chunks, first_descriptor_phys_addr,
// last_descriptor_phys_addr)
//
// This function creates one descriptor per block. For MM2S transfers every
// block will be output as a packet. For S2MM transfers the user should make
// sure n_blocks is equal to the number of AXIS packets which will be received
// by the DMA engine. Also in the S2MM case, the user should make sure that
// block_size is greater than or equal to the size of the largest AXIS packet.
fn write_regular_blocks_descriptor_chain(
    device: &(impl SGAndMCDeviceSpecifics + DeviceSpecifics),
    block_size: &usize, block_stride: &usize, n_blocks: &usize,
    offset: &usize) -> Result<(usize, usize, u64, u64), DmaError> {

    if !device.halted() {
        // Device is not halted and so we cannot modify the descriptors
        return Err(DmaError::DeviceNotHalted);
    }

    let dma_data_size = device.dma_data_size();
    let dma_descriptors_phys_addr = *device.dma_descriptors_phys_addr();
    let available_descriptor_chunks =
        device.available_descriptor_chunks();
    let n_descriptors_per_chunk = device.n_descriptors_per_chunk();

    // Check that the block size is valid. It must be a multiple of 8 and
    // in the range 8 -> max_bytes_transfer_per_descriptor. This means
    // the block size cannot be bigger than a descriptors worth of data.
    if *block_size < 8 ||
        block_size > device.max_bytes_transfer_per_descriptor() ||
        *block_size % 8 != 0 {
            return Err(DmaError::InvalidSize);
        }

    // Check that the offset is valid. Offset cannot be less than 0 due to
    // type limits.
    if offset >= dma_data_size || *offset % 8 != 0 {
        return Err(DmaError::InvalidOffset);
    }

    // Check that block_stride is greater than block_size and a multiple
    // of 8 and less than dma_data_size
    if block_stride < block_size ||
        block_stride >= dma_data_size || *block_stride % 8 != 0 {
        return Err(DmaError::InvalidStride);
    }

    // Check that a valid number of n_blocks have been requested
    let max_n_blocks = dma_data_size/block_stride;
    if *n_blocks > max_n_blocks {
        return Err(DmaError::InvalidNBlocks);
    }

    // Check that the combination of offset, stride, n blocks, block size
    // won't cause an overflow
    //
    // offset + block_stride*(n_blocks - 1) Gives the offset of the last
    // block.
    //
    // We then add block_size - 1 to get the upper_address (need to
    // subtract 1 as offset counts from 0)
    let upper_address =
        offset + block_stride*(n_blocks - 1 as usize) + (block_size - 1);
    if upper_address >= *dma_data_size {
        return Err(DmaError::MemoryOverflow);
    }

    // We have specified that we can only have one descriptor per block
    let n_descriptors = *n_blocks;
    // Calulate how many descriptor chunks are required to accomodate that
    // number of descriptors
    let n_descriptor_chunks = ceil(
        (n_descriptors as f64)/
        (*n_descriptors_per_chunk as f64), 0) as usize;

    let n_available_descriptor_chunks =
        available_descriptor_chunks.try_borrow()?.len();

    if n_descriptor_chunks > n_available_descriptor_chunks {
        // Check that the device has enough available descriptor
        // chunks to set up the requested operation. If not error.
        return Err(DmaError::InsufficientDescChunks);
    }

    let mut descriptor_chunk_index = 0;
    let mut next_descriptor_offset: u64 =
        available_descriptor_chunks.try_borrow()?[0] as u64;
    let first_descriptor_offset = next_descriptor_offset;
    let first_descriptor_phys_addr: u64 =
        first_descriptor_offset +
        dma_descriptors_phys_addr as u64;
    let mut last_descriptor_phys_addr: u64 = 0;

    // There is one descriptor per packet expected so set start of frame
    // and end of frame on every descriptor.
    let start_of_frame: u32 = device.start_of_frame_bitmask();
    let end_of_frame: u32 = device.end_of_frame_bitmask();

    // Iterate over the descriptors
    for descriptor_num in 0..n_descriptors {

        // Count the descriptors used in the chunk.
        let chunk_descriptor_num =
            descriptor_num % n_descriptors_per_chunk;

        // Set current descriptor offset equal to the next descriptor
        // offset. Next descriptor offset will be updated below for this
        // descriptor.
        let current_descriptor_offset: u64 = next_descriptor_offset;

        if descriptor_num == n_descriptors - 1 {
            // Creating the last descriptor in the chain. Note: the
            // descriptor can be both the first and last in the chain.

            // Work out the physical address of the last descriptor
            last_descriptor_phys_addr =
                current_descriptor_offset +
                dma_descriptors_phys_addr as u64;

            // Set the next descriptor offset to point to the first
            // descriptor
            next_descriptor_offset = first_descriptor_offset;
        }
        else if chunk_descriptor_num == n_descriptors_per_chunk - 1 {
            // If we are on the last descriptor in the chunk then the
            // next descriptor offset needs to be in the next
            // descriptor chunk.
            descriptor_chunk_index += 1;

            // Check that next descriptor chunk index is valid.
            if descriptor_chunk_index < n_descriptor_chunks {
                // Valid so get the next descriptor chunk offset
                next_descriptor_offset =
                    available_descriptor_chunks
                    .try_borrow()?[descriptor_chunk_index] as u64;
            }
        }
        else {
            // Increment the next descriptor offset by the descriptor
            // stride
            next_descriptor_offset += *device.descriptor_stride() as u64;
        }

        // Calculate the physical address of the next descriptor.
        let next_descriptor_phys_addr: u64 =
            dma_descriptors_phys_addr as u64 + next_descriptor_offset;

        // Calculate the physical address of the destination location
        let buffer_phys_addr: u64 =
            (device.dma_data_phys_addr() + offset +
            descriptor_num * block_stride) as u64;

        // Create the control word
        let control: u32 =
            (*block_size as u32) | start_of_frame | end_of_frame;

        // Create the descriptor. We create a vector of zeros which is
        // descriptor size in length. Note: we need to divide by 4 as the
        // descriptor size is given in bytes but we are dealing in 32 bit
        // words here
        let mut descriptor = vec![0_u32; device.descriptor_size() >> 2];

        // Populate the descriptor with the relevant fields. Note: We need
        // to divide the offsets by 4 as they are given as byte offsets
        // but we are dealing in 32 bit words here.
        descriptor[device.desc_field_nxt_desc_lsw_offset() >> 2] =
            next_descriptor_phys_addr as u32;
        descriptor[device.desc_field_nxt_desc_msw_offset() >> 2] =
            (next_descriptor_phys_addr << 32) as u32;
        descriptor[device.desc_field_buffer_addr_lsw_offset() >> 2] =
            buffer_phys_addr as u32;
        descriptor[device.desc_field_buffer_addr_msw_offset() >> 2] =
            (buffer_phys_addr << 32) as u32;
        descriptor[device.desc_field_control_offset() >> 2] =
            control as u32;

        // Need the memory fences here to prevent the cpu from reordering
        // the non volatile reads/writes around volatile reads/writes
        fence(Ordering::SeqCst);
        unsafe {
            // We can use copy_nonoverlapping here because we do not have
            // any other volatile reads within the fences.
            // Note: We divide the current_descriptor_offset by 4 as this
            // offset is counted in words whereas current descriptor
            // offset is in bytes.
            copy_nonoverlapping(
                descriptor.as_ptr(),
                device.dma_descriptors().offset(
                    (current_descriptor_offset >> 2) as isize),
                descriptor.len());
        }
        fence(Ordering::SeqCst);

    }

    Ok((n_descriptors, n_descriptor_chunks, first_descriptor_phys_addr,
        last_descriptor_phys_addr))
}

// A function to walk the descriptor chain. Checking the status of the
// descriptors, summing the number of bytes transferred and refreshing the
// descriptors. If the descriptor is reporting an error, this function will
// return that error. If none of the descriptors are reporting an error then
// this function will return the number of bytes transferred. This function
// will always refresh all descriptors in the chain even if it encounters an
// error.
fn walk_descriptor_chain_check_and_refresh(
    device: &(impl SGAndMCDeviceSpecifics + DeviceSpecifics),
    operation: &Operation)
    -> Result<usize, DmaError> {

        if !device.halted() {
            // Device is not halted so we cannot modify the descriptors
            return Err(DmaError::DeviceNotHalted);
        }

        let mut error: Option<DmaError> = None;

        // Get the device specific values
        let n_descriptors_per_chunk = device.n_descriptors_per_chunk();
        let descriptor_stride = device.descriptor_stride();

        // Get the number of descriptors from the operation
        let n_descriptors = operation.n_descriptors();

        let mut descriptor_chunk_index = 0;

        // Get the descriptor chunks out of the operation
        let operation_descriptor_chunks =
            operation.descriptor_chunk_offsets();
        let n_descriptor_chunks = operation_descriptor_chunks.len();

        // Get the offset of the first descriptor from the operation
        let mut next_descriptor_offset: usize =
            operation_descriptor_chunks[0];

        let mut nbytes: usize = 0;

        // Iterate over the descriptors
        for descriptor_num in 0..*n_descriptors {

            // Set the offset of the descriptor.
            let descriptor_offset = next_descriptor_offset;

            // Count the descriptors used in the chunk.
            let chunk_descriptor_num =
                descriptor_num % n_descriptors_per_chunk;

            if chunk_descriptor_num == n_descriptors_per_chunk - 1 {
                // If we are on the last descriptor in the chunk then the
                // next descriptor offset needs to be in the next
                // descriptor chunk.
                descriptor_chunk_index += 1;

                // Check that next descriptor chunk index is valid.
                if descriptor_chunk_index < n_descriptor_chunks {
                    // Valid so get the next descriptor chunk offset
                    next_descriptor_offset =
                        operation_descriptor_chunks[descriptor_chunk_index];
                }
            }
            else {
                // Increment the next descriptor offset by the descriptor
                // stride
                next_descriptor_offset += descriptor_stride;
            }

            if error.is_none() {
                // If we have not yet encountered an error, check the
                // descriptor status. If an error is encountered on this
                // descriptor, store it to return. Otherwise sum the nbytes
                // transferred. Note we only return the first error we
                // encounter in the decriptor chain. Also note that we don't
                // just return the error immediately here because we want to
                // finish refreshing the descriptor chain even if it has
                // errored
                match device.descriptor_status(descriptor_offset) {
                    Err(desc_err) => error = Some(desc_err),
                    Ok(desc_nbytes) => nbytes += desc_nbytes,
                }
            }

            // Overwrite the status register to clear the complete bit
            device.refresh_descriptor(descriptor_offset);

        }

        if error.is_none() {
            // No error was encountered so return the number of bytes
            // transferred
            Ok(nbytes)
        }
        else {
            // Error was encountered so return it
            return Err(error.unwrap());
        }
}

// This function checks if the device is a multi channel device
fn check_multi_channel_device(cfg: &*mut u32) -> Result<bool, DmaError> {

    // if the device is multi channel then either multi channel MM2S or multi
    // channel S2MM must be enabled. If either of these direction checks
    // returns true then it must be a multi channel device.
    // If both of them return false then it must not be a multi channel
    // device.
    Ok(multi_channel::check_mc_transfer_type(cfg, TransferType::MM2S)? ||
        multi_channel::check_mc_transfer_type(cfg, TransferType::S2MM)?)
}

#[cfg(test)]
mod tests {
    use std::{
        path::PathBuf,
    };

    use super::{
        DmaError,
        walk_descriptor_chain_check_and_refresh,
    };

    /// The `dma::walk_descriptor_chain_check_and_refresh` function should
    /// check that the device is halted before modifying the descriptors. If
    /// the device is not halted, it should return a
    /// `DmaError::DeviceNotHalted` error.
    #[test]
    fn test_walk_desc_chain_device_not_halted() -> Result<(), String> {

        let block_size: usize = 1024;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Create device
        let device_path: PathBuf =
            ["/dev", "axi_dma_sg_mm2s"].iter().collect();
        let mut device =
            super::uni_channel::scatter_gather::Device::new(&device_path)
            .unwrap();

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Trigger operation
        device.do_dma(&operation).unwrap();

        // We have not called wait for complete so device should still be
        // running which should cause the expected error
        let walk_desc_chain_result =
            walk_descriptor_chain_check_and_refresh(&device, &operation);

        match walk_desc_chain_result {
            Err(DmaError::DeviceNotHalted) => Ok(()),
            _ => Err(String::from(
                    "Did not get DmaError::DeviceNotHalted error.")),
        }
    }
}
