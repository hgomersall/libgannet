use crate::{
    errors::DmaError,
    multi_channel::registers::Registers,
    TransferType,
};

pub const N_CHANNELS: u8 = 16;

// Return the correct offset for the channel control register based on
// transfer type and channel.
pub fn ch_control_reg_offset(
    transfer_type: &TransferType, channel: &u8)-> Result<isize, DmaError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0Control.offset()),
                1 => Ok(Registers::Mm2sCh1Control.offset()),
                2 => Ok(Registers::Mm2sCh2Control.offset()),
                3 => Ok(Registers::Mm2sCh3Control.offset()),
                4 => Ok(Registers::Mm2sCh4Control.offset()),
                5 => Ok(Registers::Mm2sCh5Control.offset()),
                6 => Ok(Registers::Mm2sCh6Control.offset()),
                7 => Ok(Registers::Mm2sCh7Control.offset()),
                8 => Ok(Registers::Mm2sCh8Control.offset()),
                9 => Ok(Registers::Mm2sCh9Control.offset()),
                10 => Ok(Registers::Mm2sCh10Control.offset()),
                11 => Ok(Registers::Mm2sCh11Control.offset()),
                12 => Ok(Registers::Mm2sCh12Control.offset()),
                13 => Ok(Registers::Mm2sCh13Control.offset()),
                14 => Ok(Registers::Mm2sCh14Control.offset()),
                15 => Ok(Registers::Mm2sCh15Control.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0Control.offset()),
                1 => Ok(Registers::S2mmCh1Control.offset()),
                2 => Ok(Registers::S2mmCh2Control.offset()),
                3 => Ok(Registers::S2mmCh3Control.offset()),
                4 => Ok(Registers::S2mmCh4Control.offset()),
                5 => Ok(Registers::S2mmCh5Control.offset()),
                6 => Ok(Registers::S2mmCh6Control.offset()),
                7 => Ok(Registers::S2mmCh7Control.offset()),
                8 => Ok(Registers::S2mmCh8Control.offset()),
                9 => Ok(Registers::S2mmCh9Control.offset()),
                10 => Ok(Registers::S2mmCh10Control.offset()),
                11 => Ok(Registers::S2mmCh11Control.offset()),
                12 => Ok(Registers::S2mmCh12Control.offset()),
                13 => Ok(Registers::S2mmCh13Control.offset()),
                14 => Ok(Registers::S2mmCh14Control.offset()),
                15 => Ok(Registers::S2mmCh15Control.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        }
    }
}

// Return the correct offset for the channel status register based on
// transfer type and channel.
pub fn ch_status_reg_offset(
    transfer_type: &TransferType, channel: &u8)-> Result<isize, DmaError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0Status.offset()),
                1 => Ok(Registers::Mm2sCh1Status.offset()),
                2 => Ok(Registers::Mm2sCh2Status.offset()),
                3 => Ok(Registers::Mm2sCh3Status.offset()),
                4 => Ok(Registers::Mm2sCh4Status.offset()),
                5 => Ok(Registers::Mm2sCh5Status.offset()),
                6 => Ok(Registers::Mm2sCh6Status.offset()),
                7 => Ok(Registers::Mm2sCh7Status.offset()),
                8 => Ok(Registers::Mm2sCh8Status.offset()),
                9 => Ok(Registers::Mm2sCh9Status.offset()),
                10 => Ok(Registers::Mm2sCh10Status.offset()),
                11 => Ok(Registers::Mm2sCh11Status.offset()),
                12 => Ok(Registers::Mm2sCh12Status.offset()),
                13 => Ok(Registers::Mm2sCh13Status.offset()),
                14 => Ok(Registers::Mm2sCh14Status.offset()),
                15 => Ok(Registers::Mm2sCh15Status.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0Status.offset()),
                1 => Ok(Registers::S2mmCh1Status.offset()),
                2 => Ok(Registers::S2mmCh2Status.offset()),
                3 => Ok(Registers::S2mmCh3Status.offset()),
                4 => Ok(Registers::S2mmCh4Status.offset()),
                5 => Ok(Registers::S2mmCh5Status.offset()),
                6 => Ok(Registers::S2mmCh6Status.offset()),
                7 => Ok(Registers::S2mmCh7Status.offset()),
                8 => Ok(Registers::S2mmCh8Status.offset()),
                9 => Ok(Registers::S2mmCh9Status.offset()),
                10 => Ok(Registers::S2mmCh10Status.offset()),
                11 => Ok(Registers::S2mmCh11Status.offset()),
                12 => Ok(Registers::S2mmCh12Status.offset()),
                13 => Ok(Registers::S2mmCh13Status.offset()),
                14 => Ok(Registers::S2mmCh14Status.offset()),
                15 => Ok(Registers::S2mmCh15Status.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        }
    }
}

// Return the correct offset for the channel current descriptor LSW register
// based on transfer type and channel.
pub fn ch_current_descriptor_lsw_reg_offset(
    transfer_type: &TransferType, channel: &u8)-> Result<isize, DmaError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0CurrentDescriptorLsw.offset()),
                1 => Ok(Registers::Mm2sCh1CurrentDescriptorLsw.offset()),
                2 => Ok(Registers::Mm2sCh2CurrentDescriptorLsw.offset()),
                3 => Ok(Registers::Mm2sCh3CurrentDescriptorLsw.offset()),
                4 => Ok(Registers::Mm2sCh4CurrentDescriptorLsw.offset()),
                5 => Ok(Registers::Mm2sCh5CurrentDescriptorLsw.offset()),
                6 => Ok(Registers::Mm2sCh6CurrentDescriptorLsw.offset()),
                7 => Ok(Registers::Mm2sCh7CurrentDescriptorLsw.offset()),
                8 => Ok(Registers::Mm2sCh8CurrentDescriptorLsw.offset()),
                9 => Ok(Registers::Mm2sCh9CurrentDescriptorLsw.offset()),
                10 => Ok(Registers::Mm2sCh10CurrentDescriptorLsw.offset()),
                11 => Ok(Registers::Mm2sCh11CurrentDescriptorLsw.offset()),
                12 => Ok(Registers::Mm2sCh12CurrentDescriptorLsw.offset()),
                13 => Ok(Registers::Mm2sCh13CurrentDescriptorLsw.offset()),
                14 => Ok(Registers::Mm2sCh14CurrentDescriptorLsw.offset()),
                15 => Ok(Registers::Mm2sCh15CurrentDescriptorLsw.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0CurrentDescriptorLsw.offset()),
                1 => Ok(Registers::S2mmCh1CurrentDescriptorLsw.offset()),
                2 => Ok(Registers::S2mmCh2CurrentDescriptorLsw.offset()),
                3 => Ok(Registers::S2mmCh3CurrentDescriptorLsw.offset()),
                4 => Ok(Registers::S2mmCh4CurrentDescriptorLsw.offset()),
                5 => Ok(Registers::S2mmCh5CurrentDescriptorLsw.offset()),
                6 => Ok(Registers::S2mmCh6CurrentDescriptorLsw.offset()),
                7 => Ok(Registers::S2mmCh7CurrentDescriptorLsw.offset()),
                8 => Ok(Registers::S2mmCh8CurrentDescriptorLsw.offset()),
                9 => Ok(Registers::S2mmCh9CurrentDescriptorLsw.offset()),
                10 => Ok(Registers::S2mmCh10CurrentDescriptorLsw.offset()),
                11 => Ok(Registers::S2mmCh11CurrentDescriptorLsw.offset()),
                12 => Ok(Registers::S2mmCh12CurrentDescriptorLsw.offset()),
                13 => Ok(Registers::S2mmCh13CurrentDescriptorLsw.offset()),
                14 => Ok(Registers::S2mmCh14CurrentDescriptorLsw.offset()),
                15 => Ok(Registers::S2mmCh15CurrentDescriptorLsw.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        }
    }
}

// Return the correct offset for the channel current descriptor MSW register
// based on transfer type and channel.
pub fn ch_current_descriptor_msw_reg_offset(
    transfer_type: &TransferType, channel: &u8)-> Result<isize, DmaError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0CurrentDescriptorMsw.offset()),
                1 => Ok(Registers::Mm2sCh1CurrentDescriptorMsw.offset()),
                2 => Ok(Registers::Mm2sCh2CurrentDescriptorMsw.offset()),
                3 => Ok(Registers::Mm2sCh3CurrentDescriptorMsw.offset()),
                4 => Ok(Registers::Mm2sCh4CurrentDescriptorMsw.offset()),
                5 => Ok(Registers::Mm2sCh5CurrentDescriptorMsw.offset()),
                6 => Ok(Registers::Mm2sCh6CurrentDescriptorMsw.offset()),
                7 => Ok(Registers::Mm2sCh7CurrentDescriptorMsw.offset()),
                8 => Ok(Registers::Mm2sCh8CurrentDescriptorMsw.offset()),
                9 => Ok(Registers::Mm2sCh9CurrentDescriptorMsw.offset()),
                10 => Ok(Registers::Mm2sCh10CurrentDescriptorMsw.offset()),
                11 => Ok(Registers::Mm2sCh11CurrentDescriptorMsw.offset()),
                12 => Ok(Registers::Mm2sCh12CurrentDescriptorMsw.offset()),
                13 => Ok(Registers::Mm2sCh13CurrentDescriptorMsw.offset()),
                14 => Ok(Registers::Mm2sCh14CurrentDescriptorMsw.offset()),
                15 => Ok(Registers::Mm2sCh15CurrentDescriptorMsw.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0CurrentDescriptorMsw.offset()),
                1 => Ok(Registers::S2mmCh1CurrentDescriptorMsw.offset()),
                2 => Ok(Registers::S2mmCh2CurrentDescriptorMsw.offset()),
                3 => Ok(Registers::S2mmCh3CurrentDescriptorMsw.offset()),
                4 => Ok(Registers::S2mmCh4CurrentDescriptorMsw.offset()),
                5 => Ok(Registers::S2mmCh5CurrentDescriptorMsw.offset()),
                6 => Ok(Registers::S2mmCh6CurrentDescriptorMsw.offset()),
                7 => Ok(Registers::S2mmCh7CurrentDescriptorMsw.offset()),
                8 => Ok(Registers::S2mmCh8CurrentDescriptorMsw.offset()),
                9 => Ok(Registers::S2mmCh9CurrentDescriptorMsw.offset()),
                10 => Ok(Registers::S2mmCh10CurrentDescriptorMsw.offset()),
                11 => Ok(Registers::S2mmCh11CurrentDescriptorMsw.offset()),
                12 => Ok(Registers::S2mmCh12CurrentDescriptorMsw.offset()),
                13 => Ok(Registers::S2mmCh13CurrentDescriptorMsw.offset()),
                14 => Ok(Registers::S2mmCh14CurrentDescriptorMsw.offset()),
                15 => Ok(Registers::S2mmCh15CurrentDescriptorMsw.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        }
    }
}

// Return the correct offset for the channel tail descriptor LSW register
// based on transfer type and channel.
pub fn ch_tail_descriptor_lsw_reg_offset(
    transfer_type: &TransferType, channel: &u8)-> Result<isize, DmaError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0TailDescriptorLsw.offset()),
                1 => Ok(Registers::Mm2sCh1TailDescriptorLsw.offset()),
                2 => Ok(Registers::Mm2sCh2TailDescriptorLsw.offset()),
                3 => Ok(Registers::Mm2sCh3TailDescriptorLsw.offset()),
                4 => Ok(Registers::Mm2sCh4TailDescriptorLsw.offset()),
                5 => Ok(Registers::Mm2sCh5TailDescriptorLsw.offset()),
                6 => Ok(Registers::Mm2sCh6TailDescriptorLsw.offset()),
                7 => Ok(Registers::Mm2sCh7TailDescriptorLsw.offset()),
                8 => Ok(Registers::Mm2sCh8TailDescriptorLsw.offset()),
                9 => Ok(Registers::Mm2sCh9TailDescriptorLsw.offset()),
                10 => Ok(Registers::Mm2sCh10TailDescriptorLsw.offset()),
                11 => Ok(Registers::Mm2sCh11TailDescriptorLsw.offset()),
                12 => Ok(Registers::Mm2sCh12TailDescriptorLsw.offset()),
                13 => Ok(Registers::Mm2sCh13TailDescriptorLsw.offset()),
                14 => Ok(Registers::Mm2sCh14TailDescriptorLsw.offset()),
                15 => Ok(Registers::Mm2sCh15TailDescriptorLsw.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0TailDescriptorLsw.offset()),
                1 => Ok(Registers::S2mmCh1TailDescriptorLsw.offset()),
                2 => Ok(Registers::S2mmCh2TailDescriptorLsw.offset()),
                3 => Ok(Registers::S2mmCh3TailDescriptorLsw.offset()),
                4 => Ok(Registers::S2mmCh4TailDescriptorLsw.offset()),
                5 => Ok(Registers::S2mmCh5TailDescriptorLsw.offset()),
                6 => Ok(Registers::S2mmCh6TailDescriptorLsw.offset()),
                7 => Ok(Registers::S2mmCh7TailDescriptorLsw.offset()),
                8 => Ok(Registers::S2mmCh8TailDescriptorLsw.offset()),
                9 => Ok(Registers::S2mmCh9TailDescriptorLsw.offset()),
                10 => Ok(Registers::S2mmCh10TailDescriptorLsw.offset()),
                11 => Ok(Registers::S2mmCh11TailDescriptorLsw.offset()),
                12 => Ok(Registers::S2mmCh12TailDescriptorLsw.offset()),
                13 => Ok(Registers::S2mmCh13TailDescriptorLsw.offset()),
                14 => Ok(Registers::S2mmCh14TailDescriptorLsw.offset()),
                15 => Ok(Registers::S2mmCh15TailDescriptorLsw.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        }
    }
}

// Return the correct offset for the channel tail descriptor MSW register
// based on transfer type and channel.
pub fn ch_tail_descriptor_msw_reg_offset(
    transfer_type: &TransferType, channel: &u8)-> Result<isize, DmaError> {

    match transfer_type {
        TransferType::MM2S => {
            match channel {
                0 => Ok(Registers::Mm2sCh0TailDescriptorMsw.offset()),
                1 => Ok(Registers::Mm2sCh1TailDescriptorMsw.offset()),
                2 => Ok(Registers::Mm2sCh2TailDescriptorMsw.offset()),
                3 => Ok(Registers::Mm2sCh3TailDescriptorMsw.offset()),
                4 => Ok(Registers::Mm2sCh4TailDescriptorMsw.offset()),
                5 => Ok(Registers::Mm2sCh5TailDescriptorMsw.offset()),
                6 => Ok(Registers::Mm2sCh6TailDescriptorMsw.offset()),
                7 => Ok(Registers::Mm2sCh7TailDescriptorMsw.offset()),
                8 => Ok(Registers::Mm2sCh8TailDescriptorMsw.offset()),
                9 => Ok(Registers::Mm2sCh9TailDescriptorMsw.offset()),
                10 => Ok(Registers::Mm2sCh10TailDescriptorMsw.offset()),
                11 => Ok(Registers::Mm2sCh11TailDescriptorMsw.offset()),
                12 => Ok(Registers::Mm2sCh12TailDescriptorMsw.offset()),
                13 => Ok(Registers::Mm2sCh13TailDescriptorMsw.offset()),
                14 => Ok(Registers::Mm2sCh14TailDescriptorMsw.offset()),
                15 => Ok(Registers::Mm2sCh15TailDescriptorMsw.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        },
        TransferType::S2MM => {
            match channel {
                0 => Ok(Registers::S2mmCh0TailDescriptorMsw.offset()),
                1 => Ok(Registers::S2mmCh1TailDescriptorMsw.offset()),
                2 => Ok(Registers::S2mmCh2TailDescriptorMsw.offset()),
                3 => Ok(Registers::S2mmCh3TailDescriptorMsw.offset()),
                4 => Ok(Registers::S2mmCh4TailDescriptorMsw.offset()),
                5 => Ok(Registers::S2mmCh5TailDescriptorMsw.offset()),
                6 => Ok(Registers::S2mmCh6TailDescriptorMsw.offset()),
                7 => Ok(Registers::S2mmCh7TailDescriptorMsw.offset()),
                8 => Ok(Registers::S2mmCh8TailDescriptorMsw.offset()),
                9 => Ok(Registers::S2mmCh9TailDescriptorMsw.offset()),
                10 => Ok(Registers::S2mmCh10TailDescriptorMsw.offset()),
                11 => Ok(Registers::S2mmCh11TailDescriptorMsw.offset()),
                12 => Ok(Registers::S2mmCh12TailDescriptorMsw.offset()),
                13 => Ok(Registers::S2mmCh13TailDescriptorMsw.offset()),
                14 => Ok(Registers::S2mmCh14TailDescriptorMsw.offset()),
                15 => Ok(Registers::S2mmCh15TailDescriptorMsw.offset()),
                _ => return Err(DmaError::InvalidChannel),
            }
        }
    }
}
