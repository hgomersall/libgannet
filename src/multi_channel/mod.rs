pub mod channels;
pub mod descriptors;
mod registers;

use std::{
    cell::RefCell,
    collections::HashMap,
    fs::File,
    fs::OpenOptions,
    path::PathBuf,
    rc::Rc,
    time,
};

use memmap::{
    MmapMut,
};

use channels::{
    ch_control_reg_offset,
    ch_current_descriptor_lsw_reg_offset,
    ch_current_descriptor_msw_reg_offset,
    ch_status_reg_offset,
    ch_tail_descriptor_lsw_reg_offset,
    ch_tail_descriptor_msw_reg_offset,
    N_CHANNELS,
};

use registers::{
    CommonControlRegister,
    CommonStatusRegister,
    ErrorRegister,
    Mm2sChannelControl,
    Mm2sChannelStatus,
    Registers,
    S2mmChannelControl,
    S2mmChannelStatus,
};

use descriptors::{
    DESCRIPTOR_CHUNK_SIZE,
    DESCRIPTOR_SIZE,
    DESCRIPTOR_STRIDE,
    MAX_BYTES_TRANSFER_PER_DESCRIPTOR,
    Mm2sDescriptorControlField,
    Mm2sDescriptorFields,
    Mm2sDescriptorStatusField,
    N_DESCRIPTORS_PER_CHUNK,
    S2mmDescriptorFields,
    S2mmDescriptorStatusField,
};

use crate::{
    check_multi_channel_device,
    DeviceSpecifics,
    enable_interrupts,
    errors::DmaError,
    get_and_consume_interrupts,
    Operation,
    read_reg,
    reset,
    setup_cfg_and_data_mem,
    setup_descriptors_mem,
    SGAndMCDeviceSpecifics,
    TransferType,
    wait_for_halted,
    wait_for_idle,
    walk_descriptor_chain_check_and_refresh,
    write_reg,
    write_regular_blocks_descriptor_chain,
};

// This function takes a value and returns a vector containing the indices of
// the set bits in that value.
fn indices_of_set_bits(value: &u32) -> Vec<u8> {

    // Create the vector to store the indices
    let mut set_bits = vec![];

    // Create a copy of the input value that we can manipulate
    let mut input = *value as i64;

    while input != 0 {
        // This leaves only the least significant set bit.
        // ie. 1010 & -1010 => 0010
        let tmp = input & -input;
        // Add the indices of the least significant set bit
        set_bits.push(tmp.trailing_zeros() as u8);
        // Exclusively or that least significant set bit with the input value.
        // This removes it from the input.
        input ^= tmp;
    }

    set_bits
}

// This function writes to and reads the cfg registers to check if multi
// channel for either MM2S or S2MM is available
pub fn check_mc_transfer_type(
    cfg: &*mut u32, transfer_type: TransferType) -> Result<bool, DmaError> {

    // Determine which channel enable/disable register needs to be written to
    // depending on the transfer type.
    let ch_en_reg_offset = match transfer_type {
        TransferType::MM2S => Registers::Mm2sChannelEnable.offset(),
        TransferType::S2MM => Registers::S2mmChannelEnable.offset(),
    };

    // In the multi-channel device:
    //     Offset 0x008 = MM2S ChannelEnable register
    //         bit 0 is Read/Write
    //     Offset 0x508 = S2MM ChannelEnable register
    //         bit 0 is Read/Write
    //
    // In the non multi-channel device:
    //     Offset 0x008 = Mm2sSGCurrentDescriptorLsw register
    //         bit 0 is Read only
    //     Offset 0x508 = Outside device register space but less than one page
    //                    size (4096). Therefore it is safe to write to this
    //                    address as we assign a full page to each device.
    //
    // Based on the above if we can set bit 0 in the register at offsets 0x008
    // or 0x508 then the device must be multi channel and transfer_type must
    // be enabled. If we cannot then the device is either not multi channel or
    // transfer_type is not enabled on the device.
    write_reg(cfg, &ch_en_reg_offset, 1);

    // Read the channel enabled register
    if read_reg(cfg, &ch_en_reg_offset) == 1 {
        Ok(true)
    }
    else {
        Ok(false)
    }
}

// This function returns the transfer type of a multi channel device
fn get_transfer_type(cfg: &*mut u32) -> Result<TransferType, DmaError> {

    // Check if MM2S and S2MM are enabled
    let mc_mm2s_enabled = check_mc_transfer_type(cfg, TransferType::MM2S)?;
    let mc_s2mm_enabled = check_mc_transfer_type(cfg, TransferType::S2MM)?;

    if mc_mm2s_enabled && !mc_s2mm_enabled {
        Ok(TransferType::MM2S)
    }
    else if !mc_mm2s_enabled && mc_s2mm_enabled {
        Ok(TransferType::S2MM)
    }
    else {
        // libgannet cannot handle the situation when we have both S2MM and
        // MM2S enabled or the situation when we have neither.
        return Err(DmaError::UnsupportedCapability);
    }
}

/// The Device structure encapsulates all the information required for the
/// system to perform scatter gather DMA operations. There are methods on the
/// Device structure which enable these operations.
#[derive(Debug)]
pub struct Device {
    cfg: *mut u32,
    cfg_mem: MmapMut,
    dma_descriptors: *mut u32,
    dma_descriptors_mem: MmapMut,
    dma_descriptors_size: usize,
    dma_descriptors_phys_addr: usize,
    dma_data: *mut u8,
    dma_data_mem: MmapMut,
    dma_data_size: usize,
    dma_data_phys_addr: usize,
    transfer_type: TransferType,
    file: File,
    available_descriptor_chunks: Rc<RefCell<Vec<usize>>>,
    transfer_in_progress: bool,
}

impl Drop for Device {
    fn drop(&mut self) {
        // Reset the underlying device on drop. Ignore any errors as the
        // device is being dropped anyway.
        let _ = self.reset();
    }
}

// Implement the DeviceSpecifics trait
impl DeviceSpecifics for Device {

    // This function write a 1 to the device reset bit
    fn write_reset_bit(&self) {

        // Determine which control register needs to be written to depending
        // on the device capability.
        let common_ctrl_reg_offset =
            match self.transfer_type {
                TransferType::MM2S => Registers::Mm2sCommonControl.offset(),
                TransferType::S2MM => Registers::S2mmCommonControl.offset(),
            };

        // Reset the channel
        write_reg(
            &self.cfg, &common_ctrl_reg_offset,
            CommonControlRegister::Reset.bitmask());
    }

    // This function checks the status register to see if the device is halted
    fn halted(&self) -> bool {
        // Determine which status register needs to be read from depending on
        // the device capability.
        let common_status_reg_offset = match self.transfer_type {
            TransferType::MM2S => Registers::Mm2sCommonStatus.offset(),
            TransferType::S2MM => Registers::S2mmCommonStatus.offset(),
        };

        // Read the status register
        let status_reg_val: u32 = read_reg(
            &self.cfg, &common_status_reg_offset);

        if status_reg_val & CommonStatusRegister::Halted.bitmask() == 0 {
            // Device is not halted
            false
        }
        else {
            // Device is halted
            true
        }
    }

    // This function checks the status register to see if the device is idle
    fn idle(&self) -> bool {
        // Determine which status register needs to be read from depending on
        // the device capability.
        let common_status_reg_offset = match self.transfer_type {
            TransferType::MM2S => Registers::Mm2sCommonStatus.offset(),
            TransferType::S2MM => Registers::S2mmCommonStatus.offset(),
        };

        // Read the status register
        let status_reg_val: u32 = read_reg(
            &self.cfg, &common_status_reg_offset);

        if status_reg_val & CommonStatusRegister::Idle.bitmask() == 0 {
            // Device is not idle
            false
        }
        else {
            // Device is idle
            true
        }
    }

    fn check_error(&self) -> Result<(), DmaError> {

        // Determine which status register needs to be read from depending on
        // the device capability.
        let error_reg = match self.transfer_type {
            TransferType::MM2S => Registers::Mm2sError,
            TransferType::S2MM => Registers::S2mmError,
        };

        // Read the status register
        let error_reg_val: u32 = read_reg(&self.cfg, &error_reg.offset());

        // Return the error value. We only return one error value. This is not
        // ideal as it may be the case that there are multiple error types
        // set.
        if error_reg_val & ErrorRegister::DmaIntErr.bitmask() != 0 {
            return Err(DmaError::Internal);
        }
        else if error_reg_val & ErrorRegister::DmaSlvErr.bitmask() != 0 {
            return Err(DmaError::Slave);
        }
        else if error_reg_val & ErrorRegister::DmaDecErr.bitmask() != 0 {
            return Err(DmaError::Decode);
        }
        else if error_reg_val & ErrorRegister::SGIntErr.bitmask() != 0 {
            return Err(DmaError::SGInternal);
        }
        else if error_reg_val & ErrorRegister::SGSlvErr.bitmask() != 0 {
            return Err(DmaError::SGSlave);
        }
        else if error_reg_val & ErrorRegister::SGDecErr.bitmask() != 0 {
            return Err(DmaError::SGDecode);
        }
        else {
            Ok(())
        }
    }

    fn get_and_consume_interrupts(&mut self, timeout: &time::Duration)
        -> Result<usize, DmaError> {
            get_and_consume_interrupts(&mut self.file, timeout)
        }
}


// Implement the SGAndMCDeviceSpecifics trait
impl SGAndMCDeviceSpecifics for Device {

    fn dma_data_size(&self) -> &usize {
        &self.dma_data_size
    }

    fn dma_data_phys_addr(&self) -> &usize {
        &self.dma_data_phys_addr
    }

    fn dma_descriptors(&self) -> &*mut u32 {
        &self.dma_descriptors
    }

    fn dma_descriptors_phys_addr(&self) -> &usize {
        &self.dma_descriptors_phys_addr
    }

    fn available_descriptor_chunks(&self) -> &Rc<RefCell<Vec<usize>>> {
        &self.available_descriptor_chunks
    }

    fn max_bytes_transfer_per_descriptor(&self) -> &usize {
        &MAX_BYTES_TRANSFER_PER_DESCRIPTOR
    }

    fn n_descriptors_per_chunk(&self) -> &usize {
        &N_DESCRIPTORS_PER_CHUNK
    }

    fn descriptor_stride(&self) -> &usize {
        &DESCRIPTOR_STRIDE
    }

    fn descriptor_size(&self) -> &usize {
        &DESCRIPTOR_SIZE
    }

    fn start_of_frame_bitmask(&self) -> u32 {
        // Return the start of frame bitmask for MM2S transfers. SoF bit is
        // not set for S2MM transfers
        match self.transfer_type {
            TransferType::MM2S =>
                Mm2sDescriptorControlField::StartOfFrame.bitmask(),
            TransferType::S2MM => 0_u32,
        }
    }

    fn end_of_frame_bitmask(&self) -> u32 {
        // Return the end of frame bitmask for MM2S transfers. EoF bit is
        // not set for S2MM transfers
        match self.transfer_type {
            TransferType::MM2S =>
                Mm2sDescriptorControlField::EndOfFrame.bitmask(),
            TransferType::S2MM => 0_u32,
        }
    }

    fn desc_field_nxt_desc_lsw_offset(&self) -> usize {
        match self.transfer_type {
            TransferType::MM2S =>
                Mm2sDescriptorFields::NextDescriptorLsw.offset(),
            TransferType::S2MM =>
                S2mmDescriptorFields::NextDescriptorLsw.offset(),
        }
    }

    fn desc_field_nxt_desc_msw_offset(&self) -> usize {
        match self.transfer_type {
            TransferType::MM2S =>
                Mm2sDescriptorFields::NextDescriptorMsw.offset(),
            TransferType::S2MM =>
                S2mmDescriptorFields::NextDescriptorMsw.offset(),
        }
    }

    fn desc_field_buffer_addr_lsw_offset(&self) -> usize {
        match self.transfer_type {
            TransferType::MM2S =>
                Mm2sDescriptorFields::BufferAddrLsw.offset(),
            TransferType::S2MM =>
                S2mmDescriptorFields::BufferAddrLsw.offset(),
        }
    }

    fn desc_field_buffer_addr_msw_offset(&self) -> usize {
        match self.transfer_type {
            TransferType::MM2S =>
                Mm2sDescriptorFields::BufferAddrMsw.offset(),
            TransferType::S2MM =>
                S2mmDescriptorFields::BufferAddrMsw.offset(),
        }
    }

    fn desc_field_control_offset(&self) -> usize {
        match self.transfer_type {
            TransferType::MM2S =>
                Mm2sDescriptorFields::Control.offset(),
            TransferType::S2MM =>
                S2mmDescriptorFields::Control.offset(),
        }
    }

    // This function reads the descriptor status returns the correct error if
    // any have been reported. If there are no errors it returns the number of
    // bytes transferred by the descriptor
    fn descriptor_status(&self, descriptor_offset: usize)
        -> Result<usize, DmaError> {

            // Set the offset of the status register within the current
            // descriptor. We divide the status_offset by 4 as this offset is
            // counted in words rather than bytes.
            let status_offset = match self.transfer_type {
                TransferType::MM2S =>
                    ((descriptor_offset +
                      Mm2sDescriptorFields::Status.offset()) >> 2) as isize,
                TransferType::S2MM =>
                    ((descriptor_offset +
                      S2mmDescriptorFields::Status.offset()) >> 2) as isize,
            };

            // Read the descriptor status.
            let descriptor_status: u32 =
                read_reg(&self.dma_descriptors, &status_offset);

            // Check the error status of the descriptor and return the
            // relevant error.
            match self.transfer_type {
                TransferType::MM2S => {
                    if Mm2sDescriptorStatusField::InternalError.bitmask()
                        & descriptor_status != 0 {
                            return Err(DmaError::Internal);
                        }
                    if Mm2sDescriptorStatusField::SlaveError.bitmask() &
                        descriptor_status != 0 {
                            return Err(DmaError::Slave);
                        }
                    if Mm2sDescriptorStatusField::DecodeError.bitmask() &
                        descriptor_status != 0 {
                            return Err(DmaError::Decode);
                        }
                },
                TransferType::S2MM => {
                    if S2mmDescriptorStatusField::InternalError.bitmask()
                        & descriptor_status != 0 {
                            return Err(DmaError::Internal);
                        }
                    if S2mmDescriptorStatusField::SlaveError.bitmask() &
                        descriptor_status != 0 {
                            return Err(DmaError::Slave);
                        }
                    if S2mmDescriptorStatusField::DecodeError.bitmask() &
                        descriptor_status != 0 {
                            return Err(DmaError::Decode);
                        }
                }
            }

            // Return the nbytes transferred by this descriptor
            match self.transfer_type {
                TransferType::MM2S =>
                    Ok((Mm2sDescriptorStatusField::TransferredBytes.bitmask()
                     & descriptor_status) as usize),
                TransferType::S2MM =>
                    Ok((S2mmDescriptorStatusField::TransferredBytes.bitmask()
                     & descriptor_status) as usize)
            }
        }

    // This function overwrites the complete bit in the descriptor status to
    // refresh it for the next run
    fn refresh_descriptor(&self, descriptor_offset: usize) {

        // Set the offset of the status register within the current
        // descriptor. We divide the status_offset by 4 as this offset is
        // counted in words rather than bytes.
        let status_offset = match self.transfer_type {
            TransferType::MM2S =>
                ((descriptor_offset + Mm2sDescriptorFields::Status.offset())
                 >> 2) as isize,
            TransferType::S2MM =>
                ((descriptor_offset + S2mmDescriptorFields::Status.offset())
                 >> 2) as isize,
        };

        // Overwrite the status register to clear the complete bit
        write_reg(&self.dma_descriptors, &status_offset, 0_u32);
    }
}

impl Device {

    /// A function to create the device and get it ready for use.
    ///
    /// This function should be run first before attempting any DMA transfers.
    ///
    /// In the case of failure this function will return the causing error.
    pub fn new(device_path: &PathBuf) -> Result<Device, DmaError> {
        // Initialise the device
        let mut device = Device::init(&device_path)?;

        // Reset the device
        device.reset()?;

        Ok(device)
    }

    /// A function to initiate the DMA device.
    ///
    /// Given a system file, this function extracts all the required
    /// information and returns a DMA device.
    fn init(device_path: &PathBuf) -> Result<Device, DmaError> {

        // Open the device file
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(&device_path)?;

        let (cfg_mem, dma_data_mem, dma_data_size, dma_data_phys_addr) =
            setup_cfg_and_data_mem(device_path, &file)?;

        // Get a raw pointer to the configuration memory
        let cfg = cfg_mem.as_ptr() as *mut u32;

        if !check_multi_channel_device(&cfg)? {
            // Error if the device is not a multi channel device
            return Err(DmaError::UnsupportedUniChannel);
        }

        let transfer_type = get_transfer_type(&cfg)?;

        let (dma_descriptors_mem, dma_descriptors_size,
             dma_descriptors_phys_addr) =
            setup_descriptors_mem(device_path, &file)?;

        // Check that the dma descriptor memory space can accomodate at least
        // one descriptor chunk
        if DESCRIPTOR_CHUNK_SIZE > dma_descriptors_size {
            return Err(DmaError::InsufficientDescSpace)
        }

        // Get raw pointers to the dma data memory and the dma descriptors
        // memory
        let dma_data = dma_data_mem.as_ptr() as *mut u8;
        let dma_descriptors = dma_descriptors_mem.as_ptr() as *mut u32;

        // The descriptor space may not be a multiple of the chunk size. We
        // need to calculate the total size of the descriptor chunks which can
        // fit inside the descriptor memory space
        let dma_descriptor_chunks_size = dma_descriptors_size -
            (dma_descriptors_size % DESCRIPTOR_CHUNK_SIZE);

        // Create and populate the available descriptor chunk offsets. Rc
        // allows multiple owners of the data so that we any scatter gather
        // operations created on the device can have a reference to the
        // available_descriptor_chunks. This means that when that
        // operation is tidied up it can return its descriptor_chunks_offsets
        // to the available pool. RefCell allows the scatter gather operation
        // to mutably borrow an immutable reference.
        let available_descriptor_chunks =
            Rc::new(RefCell::new(Vec::new()));
        available_descriptor_chunks.borrow_mut()
            .extend((0..dma_descriptor_chunks_size)
                    .step_by(DESCRIPTOR_CHUNK_SIZE));

        let transfer_in_progress = false;

        Ok(Device {
            cfg,
            cfg_mem,
            dma_data,
            dma_data_mem,
            dma_data_size,
            dma_data_phys_addr,
            dma_descriptors,
            dma_descriptors_mem,
            dma_descriptors_size,
            dma_descriptors_phys_addr,
            transfer_type,
            file,
            available_descriptor_chunks,
            transfer_in_progress,
        })
    }

    /// Perform a reset on the device.
    ///
    /// This function will reset the DMA device, even if it is waiting for
    /// completion of a transfer.
    ///
    /// It will return `Ok` if successful or `DmaError::ResetFailed` if it is
    /// unsuccessful.
    pub fn reset(&mut self) -> Result<(), DmaError> {

        self.transfer_in_progress = false;

        reset(self)
    }

    /// A function to create a new regular blocks operation on a single
    /// channel.
    ///
    /// block_size is the size of each block in the memory after transfer
    /// block_stride is the offset difference between index 0 of block n and
    ///     index 0 of block n+1 in the memory after the transfer
    /// n_blocks is the number of blocks required
    /// offset is the offset in memory of index 0 of block 0
    ///
    /// Note: This function creates one descriptor per block so n_blocks
    /// should equal the number of AXIS packets you want to read from/write to
    /// memory.
    ///
    /// Note: For MM2S, block_size should equal the size of the AXIS packets
    /// you want to read from memory.
    ///
    /// Note: For S2MM, block_size should be greater than or equal to the size
    /// of the largest AXIS packet you want to write to memory.
    pub fn new_regular_blocks_operation(
        &mut self, block_size: &usize, block_stride: &usize, n_blocks: &usize,
        offset: &usize) -> Result<Operation, DmaError> {

        // Check that the arguments are valid and write the descriptor chain
        let (n_descriptors, n_descriptor_chunks, first_descriptor_phys_addr,
        last_descriptor_phys_addr) =
            write_regular_blocks_descriptor_chain(
                self, block_size, block_stride, n_blocks, offset,)?;

        // Create the new operation with the required number of descriptor
        // blocks assigned. We remove the descriptor chunks from the available
        // descriptor chunks list at the same time as creating the operation
        // so that in the event of an error they can be returned by the drop
        // function on the operation
        let operation =
            Operation::new(
                Rc::clone(&self.available_descriptor_chunks),
                self.available_descriptor_chunks.try_borrow_mut()?
                .drain(..n_descriptor_chunks).collect(), n_descriptors,
                first_descriptor_phys_addr, last_descriptor_phys_addr)?;

        Ok(operation)
    }

    /// A function to do a multi channel DMA transfer.
    ///
    /// This function expects a hashmap in the form:
    ///
    /// {channel: operation}
    ///
    /// Where channel is the channel number [0 -> 15 incl] and operation is
    /// the Operation to be run on that channel.
    ///
    /// This function will iterate over the operations in the hashmap and
    /// trigger those operations on the channel specified in the hashmap key.
    ///
    /// If a transfer is already in progress this function will return an
    /// error. In these circumstances the user can either reset the device or
    /// call wait_transfer_complete to wait for the transfer to finish.
    ///
    /// If the operations passed in were not created by this device this
    /// function will return ``DmaError::WrongDevice``.
    ///
    /// This function expects:
    ///
    /// n_descriptors = n_blocks = n AXIS packet to be received
    ///
    /// For this reason it will set the DMA engine to interrupt after all
    /// descriptors on the operation have completed (ie. when the expected
    /// number of AXIS packets have been received).
    ///
    /// If successful this function returns Ok.
    pub fn do_dma(&mut self, operations: &HashMap<u8, &Operation>) ->
        Result<(), DmaError> {

            // Check that the device is not currently doing a transfer. If it
            // is then error
            if self.transfer_in_progress {
                return Err(DmaError::TransferInProgress);
            }

            let mut channels_to_enable: u32 = 0;

            // Perform all the checks to before starting any transfers on any
            // channels
            for channel in operations.keys() {
                // Check that all the requested channels are valid. Note:
                // channel index starts at 0.
                if *channel >= N_CHANNELS {
                    return Err(DmaError::InvalidChannel);
                }

                // Get the operation for each channel
                let operation = operations.get(channel).unwrap();

                // Check that the operation was created by this device. If not
                // raise an error.
                if !operation.same_device(&self.available_descriptor_chunks) {
                    return Err(DmaError::WrongDevice);
                }

                // Set the bits to represent the channels that we want enabled
                channels_to_enable |= 1 << *channel;
            }

            // Get the channel numbers for the channels to enable
            let channels = indices_of_set_bits(&channels_to_enable);

            enable_interrupts(&mut self.file)?;

            // Get the common control and channel enable register offsets
            let (common_ctrl_reg_offset, channel_en_reg_offset) =
                match self.transfer_type {
                    TransferType::MM2S => (
                        Registers::Mm2sCommonControl.offset(),
                        Registers::Mm2sChannelEnable.offset(),),
                    TransferType::S2MM => (
                        Registers::S2mmCommonControl.offset(),
                        Registers::S2mmChannelEnable.offset(),),
                };

            // Set transfer in progress on the device
            self.transfer_in_progress = true;

            // Enable the channels in the channel enable reg
            write_reg(&self.cfg, &channel_en_reg_offset, channels_to_enable);

            for channel in &channels {
                // Get the operation for the specified channel
                let operation = operations.get(channel).unwrap();

                // Write the address of first descriptor for the channel
                self.channel_write_first_descriptor_addr(operation, channel)?;
            }

            // Set up the fetch first descriptor for the channel
            self.set_fetch_descriptor(&channels)?;

            // Set RunStop in common control reg
            write_reg(
                &self.cfg, &common_ctrl_reg_offset,
                CommonControlRegister::RunStop.bitmask());

            for channel in &channels {
                // Get the operation for the specified channel
                let operation = operations.get(channel).unwrap();

                // Enable the interrupts for the operation
                self.set_interrupts(operation, channel)?;
            }

            for channel in &channels {
                // Get the operation for the specified channel
                let operation = operations.get(channel).unwrap();

                // Set up the dma for the channel
                self.channel_initiate_dma(operation, channel)?;
            }

            Ok(())
        }

    // A function to write the address of the first descriptor for channel
    // specified using the operation passed in.
    fn channel_write_first_descriptor_addr(
        &mut self, operation: &Operation, channel: &u8) ->
        Result<(), DmaError> {

            // Get the channel current descriptor register offset
            let channel_current_desc_lsw_reg_offset =
                ch_current_descriptor_lsw_reg_offset(
                    &self.transfer_type, channel)?;
            let channel_current_desc_msw_reg_offset =
                ch_current_descriptor_msw_reg_offset(
                    &self.transfer_type, channel)?;

            // Set the address of the first descriptor in the transfer
            write_reg(
                &self.cfg, &channel_current_desc_lsw_reg_offset,
                *operation.first_descriptor_phys_addr() as u32);

            write_reg(
                &self.cfg, &channel_current_desc_msw_reg_offset,
                (operation.first_descriptor_phys_addr() >> 32) as u32);

            Ok(())
        }

    // A function to setup the the fetch of the first descriptor for the
    // channels specified.
    fn set_fetch_descriptor(
        &mut self, channels: &Vec<u8>) -> Result<(), DmaError> {

        // Channel control reg:
        //     Set Fetch
        let channel_control_word: u32 = match self.transfer_type {
            TransferType::MM2S => Mm2sChannelControl::Fetch.bitmask(),
            TransferType::S2MM => S2mmChannelControl::Fetch.bitmask(),
        };

        for channel in channels {
            // Get the channel ctrl register offset
            let channel_ctrl_reg_offset =
                ch_control_reg_offset(&self.transfer_type, channel)?;

            // Write the channel control reg
            write_reg(
                &self.cfg, &channel_ctrl_reg_offset, channel_control_word);
        }

        Ok(())
    }

    // A function to clear the control register for a DMA transfer on the
    // channels passed in.
    fn clear_ch_control_reg(
        &mut self, channels: &Vec<u8>) -> Result<(), DmaError> {

        for channel in channels {
            // Get the control register offset for each enabled channel
            let channel_control_reg_offset =
                ch_control_reg_offset(&self.transfer_type, &channel)?;

            // Write to the control register of each channel to set the
            // fetch bit low
            write_reg(&self.cfg, &channel_control_reg_offset, 0_u32);
        }
        Ok(())
    }

    // A function to enable the interrupts for a DMA transfer on a single
    // channel.
    fn set_interrupts(&mut self, operation: &Operation, channel: &u8)
        -> Result<(), DmaError> {

            // Channel control reg:
            //     Set Interrupt on Error ErrIrqEn
            //     Set Interrupt on Complete IocIrqEn
            //     Set Interrupt on error on other channel OtherChErrIrqEn
            //     Set Fetch
            //     Set Interrupt on Complete threshold to n_descriptors so the
            //         DMA engine waits for all descriptors to finish before
            //         sending the interrupt
            //FIXME Should S2MM interrupt on packet drop?!
            let channel_control_word: u32 = match self.transfer_type {
                TransferType::MM2S =>
                    Mm2sChannelControl::Fetch.bitmask() |
                    Mm2sChannelControl::OtherChErrIrqEn.bitmask() |
                    Mm2sChannelControl::IocIrqEn.bitmask() |
                    Mm2sChannelControl::ErrIrqEn.bitmask() |
                    (operation.n_descriptors <<
                     Mm2sChannelControl::IrqThreshold.offset()) as u32,
                TransferType::S2MM =>
                    S2mmChannelControl::Fetch.bitmask() |
                    S2mmChannelControl::OtherChErrIrqEn.bitmask() |
                    S2mmChannelControl::IocIrqEn.bitmask() |
                    S2mmChannelControl::ErrIrqEn.bitmask() |
                    (operation.n_descriptors <<
                     S2mmChannelControl::IrqThreshold.offset()) as u32,
            };

            // Get the channel ctrl register offset
            let channel_ctrl_reg_offset =
                ch_control_reg_offset(&self.transfer_type, channel)?;

            // Write to the control register for the channel to enable the
            // interrupts
            write_reg(
                &self.cfg, &channel_ctrl_reg_offset,
                channel_control_word);

            Ok(())
        }

    // A function to clear the interrupts for a DMA transfer on the channels
    // passed in.
    fn clear_interrupts(
        &mut self, channels: &Vec<u8>) -> Result<(), DmaError> {

        // Status register interrupt bits to clear:
        //     Interrupt on Error ErrIrqEn
        //     Interrupt on Complete IocIrqEn
        //     Interrupt on error on other channel OtherChErrIrqEn
        //FIXME If S2MM is set to interrupt on packet drop need to clear it here
        let status_reg_irq_clear_bits: u32 = match self.transfer_type {
            TransferType::MM2S =>
                Mm2sChannelStatus::OtherChErrIrq.bitmask() |
                Mm2sChannelStatus::IocIrq.bitmask() |
                Mm2sChannelStatus::ErrIrq.bitmask(),
            TransferType::S2MM =>
                S2mmChannelStatus::OtherChErrIrq.bitmask() |
                S2mmChannelStatus::IocIrq.bitmask() |
                S2mmChannelStatus::ErrIrq.bitmask(),
        };

        for channel in channels {
            // Get the status register offset for each channel
            let channel_status_reg_offset =
                ch_status_reg_offset(&self.transfer_type, &channel)?;

            // Write to the status register each channel to clear the
            // interrupts
            write_reg(
                &self.cfg, &channel_status_reg_offset,
                status_reg_irq_clear_bits);
        }
        Ok(())
    }

    // A function to initiate a DMA transfer on a single channel.
    //
    // This function will write to the channel specific tail descriptor
    // registers to trigger a DMA transfer.
    //
    // If successful this function returns Ok.
    fn channel_initiate_dma(&mut self, operation: &Operation, channel: &u8) ->
        Result<(), DmaError> {

            // Get the tail_descriptor register offsets
            let channel_tail_desc_lsw_reg_offset =
                ch_tail_descriptor_lsw_reg_offset(
                    &self.transfer_type, channel)?;
            let channel_tail_desc_msw_reg_offset =
                ch_tail_descriptor_msw_reg_offset(
                    &self.transfer_type, channel)?;

            // BEGIN THE OPERATION
            // Write the address of the last descriptor. This triggers the
            // transfer.
            write_reg(
                &self.cfg, &channel_tail_desc_lsw_reg_offset,
                *operation.last_descriptor_phys_addr() as u32);
            write_reg(
                &self.cfg, &channel_tail_desc_msw_reg_offset,
                (operation.last_descriptor_phys_addr() >> 32) as u32);

            Ok(())
        }

    /// A function to wait for the completion of transfers on all enabled
    /// channels. It then clears the interrupts on those channels and stops
    /// the device. It will block for up to the timeout period. If the
    /// transfers do not complete by then it will return. In this case the
    /// device should be reset before another transfer is attempted.
    ///
    /// Possible errors include: DmaError::Slave, DmaError::Decode,
    /// DmaError::Internal, DmaError::SGInternal, DmaError::SGSlave,
    /// DmaError::SGDecode, DmaError::TimedOut, DmaError::WrongDevice.
    ///
    /// Note: After this function returns the operation will be in a stale
    /// state. The user must call
    /// ``dma::multi_channel::device::check_and_refresh_operation`` after
    /// this function to refresh the operation.
    pub fn wait_transfer_complete(&mut self, timeout: &time::Duration) ->
        Result<(), DmaError> {
            // This function performs the following tasks:
            //     Check enabled reg to see which channels should interrupt
            //     Check all enabled channels have sent an interrupt
            //     Check common error reg to check for errors
            //     Check common status reg to look for all idle
            //     Write to each channel status register to clear interrupts
            //     Set the channel enabled reg to 0
            //     Write the stop bit
            //     Wait for device halted
            //     Set transfer_in_progress false

            // Determine which channel enable/disable register and interrupt
            // status register needs to be read depending on the transfer
            // type.
            let (ch_en_reg_offset, interrupt_status_reg_offset) =
                match self.transfer_type {
                    TransferType::MM2S => (
                        Registers::Mm2sChannelEnable.offset(),
                        Registers::Mm2sInterruptStatus.offset()),
                    TransferType::S2MM => (
                        Registers::S2mmChannelEnable.offset(),
                        Registers::S2mmInterruptStatus.offset()),
                };

            // Read the channel enable register
            let enabled_channels = read_reg(&self.cfg, &ch_en_reg_offset);

            // Get the channel numbers for the enabled channels
            let enabled_channel_numbers =
                indices_of_set_bits(&enabled_channels);

            // Get the time now
            let pre_interrupt_time_check = time::Instant::now();

            loop {

                // Get the time left in the timeout
                let timeout_used = pre_interrupt_time_check.elapsed();
                let timeout_remaining =
                    if timeout_used > *timeout {time::Duration::from_secs(0)}
                    else {*timeout - timeout_used};

                // An interrupt means that the DMA has completed on at least
                // one channel. If get_and_consume_interrupts returns 0 then
                // it timedout before detecting an interrupt.
                if self.get_and_consume_interrupts(&timeout_remaining)? == 0 {

                    // Check if it was a descriptor shortfall that caused the
                    // timeout
                    self.check_bd_shortfall(&enabled_channel_numbers)?;

                    // Poll timed out
                    return Err(DmaError::TimedOut);
                }

                // Need to enable the interrupts for any further interrupts.
                // This needs to be enabled before we read the interrupted
                // channels register to prevent a race condition after reading
                // the interrupted channels but before enabling interrupts
                enable_interrupts(&mut self.file)?;

                // Read which channels have interrupted
                let interrupted_channels: u32 =
                    read_reg(&self.cfg, &interrupt_status_reg_offset);

                // If all of the enabled channels have sent an interrupt then
                // move on. We need to AND the interrupted channels with the
                // enabled channels so we can handle the hypothetical
                // situation when channels which are not enabled also send an
                // interrupt
                if (interrupted_channels & enabled_channels) ==
                    enabled_channels {

                        // Do a get_and_consume_interrupts here to clear any
                        // interrupts which arrived after the previous
                        // get_and_consume_interrupts but before we read the
                        // interrupted channels reg
                        self.get_and_consume_interrupts(
                            &time::Duration::from_secs(0))?;

                        break;
                    }
            }

            // Check the error status of the DMA transfer.
            self.check_error()?;

            // Work out how much of the timeout we have remaining after some
            // was used waiting for the interrupts.
            let timeout_used = pre_interrupt_time_check.elapsed();
            let timeout_remaining =
                if timeout_used > *timeout {time::Duration::from_secs(0)}
                else {*timeout - timeout_used};

            // If we are doing an MM2S transfer we wait and check if the
            // device is idle. We need to do this as the DMA engine sends an
            // interrupt when the DMA is complete which is not necessarily
            // when the data has streamed out of the DMA engine (MM2S
            // transfers). When the device has returned to idle, it has
            // definitely finished streaming the data out.
            // S2MM transfers do not suffer from this issue as the DMA
            // finishes after the data has finished streaming in.
            match self.transfer_type {
                TransferType::MM2S =>
                    wait_for_idle(self, &timeout_remaining)?,
                TransferType::S2MM => (),
            }

            // Write to the stop bit to halt the device
            self.write_stop_bit();

            // Wait for the status register to show the device is halted. At
            // that point, we can refresh the descriptor chain.
            wait_for_halted(self, &time::Duration::from_millis(100))?;

            // Clear the interrupts
            self.clear_interrupts(&enabled_channel_numbers)?;

            // Clear the channel control registers to set the fetch bit low
            self.clear_ch_control_reg(&enabled_channel_numbers)?;

            // Disable all channels by writing 0 to the channel enable
            // register
            write_reg(&self.cfg, &ch_en_reg_offset, 0_u32);

            // Transfer has finished
            self.transfer_in_progress = false;

            Ok(())
        }

    /// A function to refresh the descriptors on an operation by overwriting
    /// the status registers with 0 thus setting the complete bit to 0.
    ///
    /// If a DMA transfer errors then `refresh_operation` can be called before
    /// attempting any more transfers. A subsequent `do_dma_scatter_gather`
    /// call without first calling `refresh_operation` will work but there
    /// will be a lag before the transfer commences whilst the do dma function
    /// refreshes the descriptor chain.
    pub fn check_and_refresh_operation(
        &mut self, operation: &Operation) -> Result<usize, DmaError> {

        // Check that the operation was created by this device. If not raise
        // an error.
        if !operation.same_device(&self.available_descriptor_chunks) {
            return Err(DmaError::WrongDevice);
        }

        // Walk the descriptor chain, checking the status of the descriptors
        // and refreshing them.
        let nbytes =
            walk_descriptor_chain_check_and_refresh(self, &operation)?;

        Ok(nbytes)
    }

    /// A function which returns a pointer to the device memory.
    ///
    /// This function returns a tuple which includes a pointer to the device
    /// memory allowing the calling program to directly write the data for the
    /// DMA transfer or directly read the data from the DMA transfer. Along
    /// with the size of the memory space (dma_data, dma_data_size).
    pub fn get_memory(&self) -> (*mut u8, usize) {
        // We are happy to return this pointer to the calling function because
        // the underlying memory has been permanently allocated to the device
        // by the kernel (This happens in the driver).
        (self.dma_data, self.dma_data_size)
    }

    /// This function writes to the stop bit in the multi channel register
    /// space.
    fn write_stop_bit(&mut self) {
        // Determine which control register needs to be written to depending
        // on the device capability.
        let ctrl_reg_offset = match self.transfer_type {
            TransferType::MM2S => Registers::Mm2sCommonControl.offset(),
            TransferType::S2MM => Registers::S2mmCommonControl.offset(),
        };

        // Set control register to 0. This is to set the run/stop bit low
        // which halts the device.
        write_reg(&self.cfg, &ctrl_reg_offset, 0_u32);
    }

    /// A function which returns the number descriptors available on the
    /// device when this function is called.
    pub fn n_available_descriptors (&self) -> Result<usize, DmaError> {
        let n_avail_desc =
            self.available_descriptor_chunks.try_borrow()?.len()*
            N_DESCRIPTORS_PER_CHUNK;

        Ok(n_avail_desc)
    }

    /// A function to check the specified channels for a BD shortfall. Note:
    /// BD shortfall is only possible on S2MM transfers
    fn check_bd_shortfall(&self, channels: &Vec<u8>) -> Result<(), DmaError> {

        match self.transfer_type {
            TransferType::MM2S => (),
            TransferType::S2MM => (
                for channel in channels {
                    // Loop over the specified channels

                    // Get the status register offset for each channel
                    let channel_status_reg_offset =
                        ch_status_reg_offset(&self.transfer_type, &channel)?;

                    // Read the status register of each channel
                    let channel_status = read_reg(
                        &self.cfg, &channel_status_reg_offset);

                    let bd_shortfall_bit =
                        S2mmChannelStatus::BDShortfall.bitmask();

                    if channel_status & bd_shortfall_bit != 0 {
                        return Err(DmaError::BDShortfall);
                    }
                }
            )
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use std::{
        collections::HashMap,
        path::PathBuf,
        cmp::min,
        time,
    };
    use rand::Rng;
    use serial_test_derive::serial;

    use super::{
        Device,
        DESCRIPTOR_STRIDE,
        indices_of_set_bits,
        N_CHANNELS,
        N_DESCRIPTORS_PER_CHUNK,
        MAX_BYTES_TRANSFER_PER_DESCRIPTOR,
    };

    use crate::errors::DmaError;

    #[test]
    #[serial]
    fn test_indices_of_set_bits() {

        // Generate a random number
        let mut rng = rand::thread_rng();
        let val: u32 = rng.gen();

        let dut_result = indices_of_set_bits(&val);

        // Create a vector to contain the expected result
        let mut expected_result = vec![];

        let mut temp = val;
        let mut index = 0;

        // Use a dissimilar, naive implementation of find set bits to test
        // the function
        while temp != 0 {
            if temp & 1 != 0 {
                expected_result.push(index);
            }
            temp = temp >> 1;
            index += 1;
        }

        assert!(expected_result == dut_result);
    }

    /// The `dma::multi_channel::device::new` function should return an
    /// UnsupportedCapability error if the underlying device is capable of
    /// performing both MM2S and S2MM.
    #[test]
    #[serial]
    fn test_unsupported_capability() {

        let device_path: PathBuf = ["/dev", "axi_dma_mc_combined"].iter()
            .collect();

        let device = Device::new(&device_path);

        match device {
            Err(DmaError::UnsupportedCapability) => (),
            _ => panic!("Did not get DmaError::UnsupportedCapability error."),
        }
    }

    /// The `dma::multi_channel::device::new` function should return an
    /// UnsupportedUniChannel error if the underlying device is not a multi
    /// channel device.
    #[test]
    #[serial]
    fn test_unsupported_uni_channel() {

        let device_path: PathBuf = ["/dev", "axi_dma_sg_s2mm"].iter()
            .collect();

        let device = Device::new(&device_path);

        match device {
            Err(DmaError::UnsupportedUniChannel) => (),
            _ => panic!("Did not get DmaError::UnsupportedUniChannel error."),
        }
    }

    /// The `dma::multi_channel::device::new_single_channl_regular_blocks_operation`
    /// function should check that the `block_size` argument is in the range
    /// 8 -> 2**26 and that it is a multiple of 8. If either of these is not
    /// true then it should return a `DmaError::InvalidSize` error.
    #[test]
    #[serial]
    fn test_invalid_size() {

        // Create the device
        let device_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = loop {
            // Generate a random block_size
            let mut rng = rand::thread_rng();
            let block_size: usize = rng.gen();

            if block_size < 8 || block_size >= 1<<26 || block_size % 8 != 0 {
                // Check that block_size is invalid, if it is then return it
                break block_size;
            }
        };

        let block_stride = block_size;
        let n_blocks = 1;
        let offset: usize = 0;

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::InvalidSize) => (),
            _ => panic!("Did not get DmaError::InvalidSize error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should check that the `offset` argument is in the range
    /// 0 -> `dma_data_size` and that it is a multiple of 8. If either of
    /// these is not true then it should return a `DmaError::InvalidOffset`
    /// error.
    #[test]
    #[serial]
    fn test_invalid_offset() {

        // Create the device
        let device_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = 16;
        let block_stride = block_size;
        let n_blocks = 1;

        let offset: usize = loop {
            // Generate a random offset
            let mut rng = rand::thread_rng();
            let offset: usize = rng.gen();

            if offset >= device.dma_data_size || offset % 8 != 0 {
                // Check that offset is invalid, if it is then return it
                break offset;
            }
        };

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::InvalidOffset) => (),
            _ => panic!("Did not get DmaError::InvalidOffset error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should check that the `block_stride` argument is in the range
    /// block_size -> `dma_data_size` and that it is a multiple of 8. If
    /// either of these is not true then it should return a
    /// `DmaError::InvalidStride` error.
    #[test]
    #[serial]
    fn test_invalid_block_stride() {

        // Create the device
        let device_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = 1024;
        let offset: usize = 0;
        let n_blocks = 1;

        let block_stride: usize = loop {
            // Generate a random block stride
            let mut rng = rand::thread_rng();
            let block_stride: usize = rng.gen();

            if block_stride < block_size ||
                block_stride >= device.dma_data_size ||
                    block_stride % 8 != 0 {
                        // Check that block_stride is invalid, if it is then
                        // return it
                        break block_stride;
                    }
        };

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::InvalidStride) => (),
            _ => panic!("Did not get DmaError::InvalidStride error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should check that the `n_blocks` argument does not exceed the
    /// maximum possible number of blocks given the block_stride. If it does
    /// then the function should return a `DmaError::InvalidNBlocks` error.
    #[test]
    #[serial]
    fn test_invalid_n_blocks() {

        // Create the device
        let device_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let block_size: usize = 1024;
        let offset: usize = 0;

        // Generate a random block stride
        let mut rng = rand::thread_rng();
        let block_stride: usize =
            8*rng.gen_range(block_size/8, device.dma_data_size/8);

        let min_n_blocks = device.dma_data_size/block_stride + 1;

        let n_blocks =
            rng.gen_range(min_n_blocks, min_n_blocks*2);

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::InvalidNBlocks) => (),
            _ => panic!("Did not get DmaError::InvalidNBlocks error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should return an InsufficientDescChunks error if the
    /// device does not have sufficient descriptor chunks available to set up
    /// the requested operation.
    #[test]
    #[serial]
    fn test_insufficient_descriptor_chunks() {

        let device_path: PathBuf = ["/dev", "axi_dma_mc_s2mm"].iter()
            .collect();

        let mut device = Device::new(&device_path).unwrap();

        let mut rng = rand::thread_rng();

        // Work out how many descriptor chunks are available on the device
        let n_descriptor_chunks_available =
            device.dma_descriptors_size/
            (N_DESCRIPTORS_PER_CHUNK * DESCRIPTOR_STRIDE);

        // Calculate the total number of available descriptors
        let total_available_descriptors =
            n_descriptor_chunks_available * N_DESCRIPTORS_PER_CHUNK;

        // Randomly select a block size which is small enough for us to set
        // n_blocks such that it will cause an insufficient descriptor chunks
        // error
        let block_size = 8*rng.gen_range(1, total_available_descriptors/8);

        // Set the block stride so that the blocks won't overlap
        let block_stride = block_size;
        let offset = 0;

        // Select the number of blocks so that the device does not have
        // enough descriptors
        let n_blocks = rng.gen_range(
            total_available_descriptors + 1,
            device.dma_data_size/block_stride + 1);

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::InsufficientDescChunks) => (),
            _ => panic!(
                "Did not get DmaError::InsufficientDescChunks error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should check that the `block_size`, `block_stride`,
    /// `n_blocks` and `offset` arguments will not cause a transfer to memory
    /// outside `dma_data`. If they do then it would cause a memory overflow.
    /// The function should prevent this by returning a
    /// `DmaError::MemoryOverflow` error.
    #[test]
    #[serial]
    fn test_memory_overflow() {

        // Create the device
        let device_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let max_block_size =
            min(device.dma_data_size, MAX_BYTES_TRANSFER_PER_DESCRIPTOR);

        let (block_size, block_stride, n_blocks, offset) = loop {
            // Generate a random nbytes
            let mut rng = rand::thread_rng();

            // Randomly select arguments
            let block_size = 8*rng.gen_range(1, max_block_size/8 - 1);
            let block_stride: usize =
                8*rng.gen_range(block_size/8, device.dma_data_size/8);

            let max_n_blocks = device.dma_data_size/block_stride;
            let n_blocks = rng.gen_range(1, max_n_blocks + 1);

            let offset: usize = 8*rng.gen_range(0, device.dma_data_size/8);

            if (offset + block_stride*(n_blocks-1) + block_size) >
                device.dma_data_size {
                    // Check that nbytes and offset would result in a memory
                    // overflow, if it would then return it
                    break (block_size, block_stride, n_blocks, offset);
                }
        };

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        match operation {
            Err(DmaError::MemoryOverflow) => (),
            _ => panic!("Did not get DmaError::MemoryOverflow error."),
        }
    }

    /// The `dma::multi_channel::device::new_regular_blocks_operation`
    /// function should check that the device is halted before modifying the
    /// descriptors. If the device is not halted, it should return a
    /// `DmaError::DeviceNotHalted` error.
    #[test]
    #[serial]
    fn test_new_operation_device_not_halted() {

        let block_size: usize = 4096;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Set up device paths
        let mm2s_device_path: PathBuf =
            ["/dev", "axi_dma_mc_mm2s"].iter().collect();
        let s2mm_device_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();

        // Create devices
        let mut mm2s_device = Device::new(&mm2s_device_path).unwrap();
        let mut s2mm_device = Device::new(&s2mm_device_path).unwrap();

        // Create the operations
        let mm2s_operation = mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();
        let s2mm_operation = s2mm_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Create the operations hashmaps
        let mut mm2s_operations = HashMap::new();
        let mut s2mm_operations = HashMap::new();
        s2mm_operations.insert(0_u8, &s2mm_operation);
        mm2s_operations.insert(0_u8, &mm2s_operation);

        // Trigger operations. Need to perform the S2MM operation as well as
        // MM2S to drain the FIFO. Need to trigger the S2MM transfer first.
        // Otherwise the channel is not enabled and the S2MM DMA engine drops
        // all of the packets before we can enable it.
        s2mm_device.do_dma(&s2mm_operations).unwrap();
        mm2s_device.do_dma(&mm2s_operations).unwrap();

        // We have not called wait for complete so mm2s_device should still be
        // running which should cause the expected error
        let new_operation_result = mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset);

        let timeout = time::Duration::from_millis(100);

        // Wait for the transfers to finish
        mm2s_device.wait_transfer_complete(&timeout).unwrap();
        s2mm_device.wait_transfer_complete(&timeout).unwrap();

        s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();
        mm2s_device.check_and_refresh_operation(&mm2s_operation).unwrap();

        match new_operation_result {
            Err(DmaError::DeviceNotHalted) => (),
            _ => panic!("Did not get DmaError::DeviceNotHalted error."),
        }
    }

    /// The `dma::multi_channel::device::do_dma` function should check that
    /// the device is not already performing a transfer. If it is then it
    /// should return `DmaError::TransferInProgress`.
    #[test]
    #[serial]
    fn test_transfer_in_progress() {

        let block_size: usize = 1024;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Set up device paths
        let mm2s_device_path: PathBuf =
            ["/dev", "axi_dma_mc_mm2s"].iter().collect();
        let s2mm_device_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();

        // Create devices
        let mut mm2s_device = Device::new(&mm2s_device_path).unwrap();
        let mut s2mm_device = Device::new(&s2mm_device_path).unwrap();

        // Create the operations
        let mm2s_operation = mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();
        let s2mm_operation = s2mm_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Create the operations hashmaps
        let mut mm2s_operations = HashMap::new();
        let mut s2mm_operations = HashMap::new();
        s2mm_operations.insert(0_u8, &s2mm_operation);
        mm2s_operations.insert(0_u8, &mm2s_operation);

        // Trigger operations. Need to perform the S2MM operation as well as
        // MM2S to drain the FIFO. Need to trigger the S2MM transfer first.
        // Otherwise the channel is not enabled and the S2MM DMA engine drops
        // all of the packets before we can enable it.
        s2mm_device.do_dma(&s2mm_operations).unwrap();
        mm2s_device.do_dma(&mm2s_operations).unwrap();

        // Try to trigger a second DMA transfer without calling
        // wait_transfer_complete or reset. This should return
        // TransferInProgress
        let do_dma_result = s2mm_device.do_dma(&s2mm_operations);

        let timeout = time::Duration::from_millis(100);

        // Wait for the transfers to finish
        mm2s_device.wait_transfer_complete(&timeout).unwrap();
        s2mm_device.wait_transfer_complete(&timeout).unwrap();

        s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();
        mm2s_device.check_and_refresh_operation(&mm2s_operation).unwrap();

        match do_dma_result {
            Err(DmaError::TransferInProgress) => (),
            _ => panic!("Did not get DmaError::TransferInProgress error."),
        }
    }

    /// The `dma::multi_channel::device::refresh_operation` function
    /// should check that the device is halted before modifying the
    /// descriptors. If the device is not halted, it should return a
    /// `DmaError::DeviceNotHalted` error.
    #[test]
    #[serial]
    fn test_refresh_operation_device_not_halted() {

        let block_size: usize = 4096;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Set up device paths
        let mm2s_device_path: PathBuf =
            ["/dev", "axi_dma_mc_mm2s"].iter().collect();
        let s2mm_device_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();

        // Create devices
        let mut mm2s_device = Device::new(&mm2s_device_path).unwrap();
        let mut s2mm_device = Device::new(&s2mm_device_path).unwrap();

        // Create the operations
        let mm2s_operation = mm2s_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();
        let s2mm_operation = s2mm_device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Create the operations hashmaps
        let mut mm2s_operations = HashMap::new();
        let mut s2mm_operations = HashMap::new();
        s2mm_operations.insert(0_u8, &s2mm_operation);
        mm2s_operations.insert(0_u8, &mm2s_operation);

        // Trigger operation. Need to perform the S2MM operation as well as
        // MM2S to drain the FIFO. Need to trigger the S2MM transfer first.
        // Otherwise the channel is not enabled and the S2MM DMA engine drops
        // all of the packets before we can enable it.
        s2mm_device.do_dma(&s2mm_operations).unwrap();
        mm2s_device.do_dma(&mm2s_operations).unwrap();

        // We have not called wait for complete so device should still be
        // running which should cause the expected error
        let refresh_operation_result =
            mm2s_device.check_and_refresh_operation(&mm2s_operation);

        let timeout = time::Duration::from_millis(100);

        // Wait for the transfers to finish
        mm2s_device.wait_transfer_complete(&timeout).unwrap();
        s2mm_device.wait_transfer_complete(&timeout).unwrap();

        s2mm_device.check_and_refresh_operation(&s2mm_operation).unwrap();
        mm2s_device.check_and_refresh_operation(&mm2s_operation).unwrap();

        match refresh_operation_result {
            Err(DmaError::DeviceNotHalted) => (),
            _ => panic!("Did not get DmaError::DeviceNotHalted error."),
        }
    }

    /// The `dma::multi_channel::device::do_dma` function should check that
    /// the `operation` argument was created by the device which is being
    /// called on to run the DMA. If it was not then it should return a
    /// `DmaError::WrongDevice` error.
    #[test]
    #[serial]
    fn test_do_dma_wrong_device() {

        let block_size: usize = 1024;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Create device 0
        let device_0_path: PathBuf =
            ["/dev", "axi_dma_mc_mm2s"].iter().collect();
        let mut device_0 = Device::new(&device_0_path).unwrap();

        // Create device 1
        let device_1_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device_1 = Device::new(&device_1_path).unwrap();

        let operation_1 = device_1.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Create the operations hashmap
        let mut operations_on_1 = HashMap::new();
        operations_on_1.insert(0_u8, &operation_1);

        // Try to run do dma on an operation which was not created by the
        // device being called
        let do_dma_result = device_0.do_dma(&mut operations_on_1);

        match do_dma_result {
            Err(DmaError::WrongDevice) => (),
            _ => panic!("Did not get DmaError::WrongDevice error."),
        }
    }

    /// The `dma::multi_channel::device::do_dma` function should check that
    /// the `channel` argument is valid (in the range 0 - 15 inclusive). If it
    /// is not then it should return `DmaError::InvalidChannel`.
    #[test]
    #[serial]
    fn test_do_dma_invalid_channel() {

        let block_size: usize = 1024;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Create device
        let device_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let operation = device.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Select a random invalid channel
        let mut rng = rand::thread_rng();
        let channel = rng.gen_range(N_CHANNELS, 255);

        // Create the operations hashmap
        let mut operations = HashMap::new();
        operations.insert(channel, &operation);

        // Try to run do dma on an operation which was not created by the
        // device being called
        let do_dma_result = device.do_dma(&operations);

        match do_dma_result {
            Err(DmaError::InvalidChannel) => (),
            _ => panic!("Did not get DmaError::InvalidChannel error."),
        }
    }

    /// The `dma::multi_channel::device::wait_transfer_complete` function
    /// should return `DmaError::TimedOut` if it does not receive an interrupt
    /// within the `timeout` period
    #[test]
    #[serial]
    fn test_wait_transfer_complete_timeout() {

        // Create device
        let device_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device = Device::new(&device_path).unwrap();

        let timeout = time::Duration::from_millis(100);

        // We have not triggered a transfer so wait_transfer_complete should
        // return DmaError::TimedOut
        let wait_transfer_complete_result =
            device.wait_transfer_complete(&timeout);

        match wait_transfer_complete_result {
            Err(DmaError::TimedOut) => (),
            _ => panic!("Did not get DmaError::TimedOut error."),
        }
    }

    /// The `dma::multi_channel::device::check_and_refresh_operation`
    /// function should check that the `operation` argument was created by the
    /// device upon which it is being called. If it was not then it should
    /// return a `DmaError::WrongDevice` error.
    #[test]
    #[serial]
    fn test_check_and_refresh_operation_wrong_device() {

        let block_size: usize = 1024;
        let block_stride = block_size;
        let offset: usize = 0;
        let n_blocks = 1;

        // Create device 0
        let device_0_path: PathBuf =
            ["/dev", "axi_dma_mc_mm2s"].iter().collect();
        let mut device_0 = Device::new(&device_0_path).unwrap();

        // Create device 1
        let device_1_path: PathBuf =
            ["/dev", "axi_dma_mc_s2mm"].iter().collect();
        let mut device_1 = Device::new(&device_1_path).unwrap();

        let operation_1 = device_1.new_regular_blocks_operation(
            &block_size, &block_stride, &n_blocks, &offset).unwrap();

        // Try to call refresh_operation on an operation which was not
        // created by the device being called
        let refresh_operation_result =
            device_0.check_and_refresh_operation(&operation_1);

        match refresh_operation_result {
            Err(DmaError::WrongDevice) => (),
            _ => panic!("Did not get DmaError::WrongDevice error."),
        }
    }
}
