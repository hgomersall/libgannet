use std::{
    cell,
    error::Error,
    fmt,
    io,
    num,
};

/// Errors                   | Meaning
/// ------------------------ | ----------------------------------------------------------------------------------------------------------------------------
/// UnsupportedCapability    | Library cannot handle MM2S and S2MM in the same device
/// UnsupportedScatterGather | Function requires a direct device and does not support scatter gather dma
/// UnsupportedDirect        | Function requires a scatter gather device and does not support direct dma
/// UnsupportedMultiChannel  | Function requires a non multi channel device and does not support multi channel DMA
/// UnsupportedUniChannel    | Function requires a multi channel device and does not support non multi channel DMA
/// ResetFailed              | Device took too long to reset
/// HaltTimedOut             | Device took too long to halt
/// TransferInProgress       | Transfer in progress. Call wait_transfer_complete
/// Internal                 | DMA Internal Error. Buffer Length is 0 or Memory write error or incoming packet is bigger than value in the length register.
/// Slave                    | DMA Slave Error. The slave read from the Memory Map interface issued a Slave Error
/// Decode                   | DMA Decode Error. Address requested is invalid.
/// SGInternal               | Scatter Gather Internal Error. This error occurs if a descriptor with the “Complete bit” already set is fetched.
/// SGSlave                  | Scatter Gather Slave Error. The slave read from the Memory Map interface issued a Slave error
/// SGDecode                 | Scatter Gather Decode Error. This error occurs if CURDESC_PTR and/or NXTDESC_PTR points to an invalid address.
/// InvalidSize              | Invalid number of bytes requested
/// InvalidOffset            | Invalid offset for the transfer
/// InvalidStride            | Invalid stride for the transfer
/// InvalidNBlocks           | Invalid number of blocks requested
/// MemoryOverflow           | The combination of nbytes and offset would cause a memory overflow
/// TimeOut                  | The DMA transfer timed out
/// InvalidMemMap            | The device memory maps have changed (order or name). This could be due to changes in the axi-irq-mem driver
/// InsufficientDescSpace    | The available descriptor space is too small for descriptor chunk size.
/// InsufficientDescChunks   | The device does not have enough descriptor chunks left for the requested operation
/// WrongDevice              | The operation was not created by this device
/// DeviceNotHalted          | Device is not halted. This operation can only run on a halted device
/// InvalidChannel           | Requested channel is outside the range 0 - 15
/// BDShortfall              | The packet being transferred has not yet set the end of frame bit but there are no more descriptors
#[derive(Debug)]
pub enum DmaError {
    Io(io::Error),
    NumParseInt(num::ParseIntError),
    CellBorrow(cell::BorrowError),
    CellBorrowMut(cell::BorrowMutError),
    Nix(nix::Error),
    UnsupportedCapability,    // Library cannot handle MM2S and S2MM in the same device
    UnsupportedScatterGather, // Function requires a direct device and does not support scatter gather dma
    UnsupportedDirect,        // Function requires a scatter gather device and does not support direct dma
    UnsupportedMultiChannel,  // Function requires a non multi channel device and does not support multi channel dma
    UnsupportedUniChannel,    // Function requires a multi channel device and does not support non multi channel dma
    ResetFailed,              // Device took too long to reset
    HaltTimedOut,             // Device took too long to halt
    TransferInProgress,       // Transfer in progress. Call wait_transfer_complete
    Internal,                 // DMA internal error
    Slave,                    // DMA slave error
    Decode,                   // DMA decode error
    SGInternal,               // Scatter Gather internal error
    SGSlave,                  // Scatter Gather slave error
    SGDecode,                 // Scatter Gather decode error
    InvalidSize,              // Invalid size requested
    InvalidOffset,            // Invalid offset for the transfer
    InvalidStride,            // Invalid stride for the transfer
    InvalidNBlocks,           // Invalid number of blocks requested
    MemoryOverflow,           // The combination of nbytes and offset would cause a memory overflow
    TimedOut,                 // The DMA transfer timed out
    InvalidMemMap,            // Library cannot handle the device memory maps
    InsufficientDescSpace,    // Descriptor space is too small for the descriptor chunk size
    InsufficientDescChunks,   // There are not enough descriptor chunks to set up the requested operation
    WrongDevice,              // Operation was not created by this device
    DeviceNotHalted,          // Device is not halted. This operation can only run on a halted device
    InvalidChannel,           // Requested channel is outside the range 0 - 15
    BDShortfall,              // Insufficient descriptors for the transfer
}

impl fmt::Display for DmaError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            DmaError::Io(ref err) => err.fmt(f),
            DmaError::NumParseInt(ref err) => err.fmt(f),
            DmaError::CellBorrow(ref err) => err.fmt(f),
            DmaError::CellBorrowMut(ref err) => err.fmt(f),
            DmaError::Nix(ref err) => err.fmt(f),
            DmaError::UnsupportedCapability => {
                write!(f, "DMA Error: UnsupportedCapability. Device must be \
                       MM2S or S2MM.")},
            DmaError::UnsupportedScatterGather => {
                write!(f, "DMA Error: UnsupportedScatterGather. Device must \
                       be a direct DMA engine.")},
            DmaError::UnsupportedDirect => {
                write!(f, "DMA Error: UnsupportedDirect. Device must be a \
                       scatter gather DMA engine.")},
            DmaError::UnsupportedMultiChannel => {
                write!(f, "DMA Error: UnsupportedMultiChannel. Device must \
                       not be a multi channel DMA engine.")},
            DmaError::UnsupportedUniChannel => {
                write!(f, "DMA Error: UnsupportedUniChannel. Device must be \
                       a multi channel DMA engine.")},
            DmaError::ResetFailed => {
                write!(f, "DMA Error: ResetFailed. Device did not reset \
                       within an expected time.")},
            DmaError::HaltTimedOut => {
                write!(f, "DMA Error: HaltTimedOut. Device did not halt \
                       within an expected time.")},
            DmaError::TransferInProgress => {
                write!(f, "DMA Error: TransferInProgress. Transfer in \
                       progress. Call wait_transfer_complete.")},
            DmaError::Internal => {
                write!(f, "DMA Error: Internal. Buffer Length is 0 or Memory \
                       write error or incoming packet is bigger than value \
                       in the length register.")},
            DmaError::Slave => {
                write!(f, "DMA Error: Slave. The slave read from the Memory \
                       Map interface issued a Slave Error")},
            DmaError::Decode => {
                write!(f, "DMA Error: Decode. Address requested is \
                       invalid.")},
            DmaError::SGInternal => {
                write!(f, "DMA Error: SGInternal. This error occurs if a \
                       descriptor with the “Complete bit” already set is \
                       fetched. This indicates to the SG Engine that the \
                       descriptor is a stale descriptor.")},
            DmaError::SGSlave => {
                write!(f, "DMA Error: SGSlave. The slave read from the \
                       Memory Map interface issued a Slave error.")},
            DmaError::SGDecode => {
                write!(f, "DMA Error: SGDecode. This error occurs if \
                       CURDESC_PTR and/or NXTDESC_PTR points to an invalid \
                       address.")},
            DmaError::InvalidSize => {
                write!(f, "DMA Error: InvalidSize. Invalid size requested.")},
            DmaError::InvalidOffset => {
                write!(f, "DMA Error: InvalidOffset. Offset must be in the \
                       range 0 to dma_data_size and a multiple of 8.")},
            DmaError::InvalidStride => {
                write!(f, "DMA Error: InvalidStride. Stride must be greater \
                       than or equal to size, less than dma_data_size and a \
                       multiple of 8.")},
            DmaError::InvalidNBlocks => {
                write!(f, "DMA Error: InvalidNBlocks. Invalid number of \
                       blocks requested.")},
            DmaError::MemoryOverflow => {
                write!(f, "DMA Error: MemoryOverflow. The combination of \
                       arguments would cause a memory overflow.")},
            DmaError::TimedOut => {
                write!(f, "DMA Error: TimedOut. The DMA transfer timed \
                       out.")},
            DmaError::InvalidMemMap => {
                write!(f, "DMA Error: InvalidMemMap. The device memory maps \
                       have changed (order or name). This could be due to \
                       changes in the axi-irq-mem driver.")},
            DmaError::InsufficientDescSpace => {
                write!(f, "DMA Error: InsufficientDescSpace. The available \
                       descriptor space is too small for descriptor chunk \
                       size.")},
            DmaError::InsufficientDescChunks => {
                write!(f, "DMA Error: InsufficientDescChunks. The device \
                       does not have enough descriptor chunks left for the \
                       requested operation.")},
            DmaError::WrongDevice => {
                write!(f, "DMA Error: WrongDevice. The operation was not \
                       created by this device.")},
            DmaError::DeviceNotHalted => {
                write!(f, "DMA Error: DeviceNotHalted. This operation can \
                       only run on a halted device.")},
            DmaError::InvalidChannel => {
                write!(f, "DMA Error: InvalidChannel. Channel must be in \
                       range 0 - 15 inclusive.")},
            DmaError::BDShortfall => {
                write!(f, "DMA Error: BDShortfall. The packet being \
                       transferred has not yet set the end of frame bit but \
                       there are no more descriptors.")},
        }
    }
}

impl Error for DmaError {
}

// Implement from traits to convert other error types which may occur within
// the library into DmaError.

impl From<io::Error> for DmaError {
    fn from(err: io::Error) -> DmaError {
        DmaError::Io(err)
    }
}

impl From<num::ParseIntError> for DmaError {
    fn from(err: num::ParseIntError) -> DmaError {
        DmaError::NumParseInt(err)
    }
}

impl From<cell::BorrowError> for DmaError {
    fn from(err: cell::BorrowError) -> DmaError {
        DmaError::CellBorrow(err)
    }
}

impl From<cell::BorrowMutError> for DmaError {
    fn from(err: cell::BorrowMutError) -> DmaError {
        DmaError::CellBorrowMut(err)
    }
}

impl From<nix::Error> for DmaError {
    fn from(err: nix::Error) -> DmaError {
        DmaError::Nix(err)
    }
}
